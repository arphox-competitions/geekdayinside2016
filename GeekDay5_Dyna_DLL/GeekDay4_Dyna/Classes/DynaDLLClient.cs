﻿using DynaCommon;
using GeekDay4_Dyna.Game;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace GeekDay4_Dyna
{
    public class DynaDLLClient : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged([CallerMemberName] string name = "")
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(name));
        }
        public event EventHandler<DynaCommand> CommandReceived;

        public IDynaClient DLL { get; set; }

        string nick = null;
        public string NickName
        {
            get
            {
                if (nick == null && DLL!=null)
                {
                    nick = Regex.Replace(DLL.NickName, "[^A-Za-z0-9 _]", "");
                }
                return nick;
            }
        }
        public BitmapImage Image
        {
            get { return DLL == null ? null : DLL.TileOfClient(); }
        }

        public string LevelXML { get; set; }
        public int LevelTimeLeft { get; set; }
        public long LevelTickNum { get; set; }

        int clientNumber;
        public int ClientNumber
        {
            get { return clientNumber; }
            set { clientNumber = value; OnPropertyChanged(); }
        }

        int cmdNumber;
        public int CmdNumber
        {
            get { return cmdNumber; }
            set { cmdNumber = value; OnPropertyChanged(); }
        }

        Thread myThread;

        public DynaDLLClient()
        {
            clientNumber = -1;
            LevelXML = String.Empty;
            myThread = new Thread(ThreadMethod);
            myThread.Start();

            //new Task(() => ThreadMethod(), TaskCreationOptions.LongRunning).Start();
            //Task.Run(() => ThreadMethod());
        }

        public void TimeLeft(int time)
        {
            if (DLL != null && LevelXML != null)
            {
                DLL.TimeLeft(time);
            }
        }

        public void Abort()
        {
            myThread.Abort();
        }

        void ThreadMethod()
        {
            long loopNum = 0;
            while (true)
            {
                loopNum++;
                if (DLL != null)
                {
                    DynaCommand cmd = DLL.CommandFromClient(Level.TICKNUM);
                    if (CommandReceived != null && cmd != DynaCommand.CmdNothing)
                    {
                        CommandReceived(this, cmd);
                        CmdNumber++;
                    }
                    if (loopNum % 5 == 0)
                    {
                        DLL.ItemsToClient(LevelXML);
                    }
                    if (loopNum % 20 == 0)
                    {
                        DLL.TimeLeft(LevelTimeLeft);
                    }
                }
                Thread.Sleep(100);
            }
        }
    }
}
