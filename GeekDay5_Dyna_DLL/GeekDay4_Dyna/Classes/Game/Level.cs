﻿using DynaCommon;
using GeekDay4_Dyna.Classes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace GeekDay4_Dyna.Game
{
    public class Level : FrameworkElement
    {
        const int GAMETIME = 180;
        const int PLAYERNUM = 18;
        const int BOMBTIME = 30;
        const int LIVES = 100;
        public int PlayerNum { get { return PLAYERNUM; } }
        public int TileNum { get; set; }
        public static long TICKNUM { get; private set; }

        Typeface TEXTFONT = new Typeface("Arial");
        Point TEXTLOC = new Point(10, 10);

        GameItem[,] FixTiles;
        List<GameItem> SecretTiles = new List<GameItem>();
        public List<Player> Players { get; set; }
        List<Bomb> Bombs = new List<Bomb>();
        List<Bomb> Flames = new List<Bomb>();
        Point[] StartPositions = new Point[18];
        DispatcherTimer RenderTimer;
        DispatcherTimer TimeCounterTimer;

        Window MyWin;
        int MyWidth, MyHeight;
        int TileSize;
        public int TimeLeft { get; set; }
        public bool IsAutoPlay { get; set; }
        public event EventHandler LevelReset;


        public Level()
        {
            Loaded += Level_Loaded;
            Players = new List<Player>();
        }

        void AddPlayer()
        {
            int num = Players.Count;
            char c = num < 10 ? num.ToString()[0] : (char)(num - 10 + 'A');
            Player item = (Player)GameItem.NewItem(c);
            item.PosX = StartPositions[num].X;
            item.PosY = StartPositions[num].Y;
            Players.Add(item);
        }
        public void Reset()
        {
            SecretTiles.Clear();
            Players.Clear();
            Bombs.Clear();
            Flames.Clear();
            TimeLeft = GAMETIME;

            MyWidth = (int)ActualWidth;
            MyHeight = (int)ActualHeight;
            int TileNum = int.Parse(ConfigurationManager.AppSettings["LEVELSIZE"].ToString());
            TileSize = Math.Min(MyWidth / TileNum, MyHeight / TileNum);

            string[] lines = File.ReadAllLines(ConfigurationManager.AppSettings["LEVEL"].ToString());
            FixTiles = new GameItem[TileNum, TileNum];
            GameItem item;
            DateTime start = DateTime.Now;
            for (int y = 0; y < TileNum; y++)
            {
                for (int x = 0; x < TileNum; x++)
                {
                    char c = lines[y][x];
                    switch (c)
                    {
                        case '0':
                        case '1':
                        case '2':
                        case '3':
                        case '4':
                        case '5':
                        case '6':
                        case '7':
                        case '8':
                        case '9':
                        case 'A':
                        case 'B':
                        case 'C':
                        case 'D':
                        case 'E':
                        case 'F':
                        case 'G':
                        case 'H':
                            int num = (c <= '9') ? c - '0' : c - 'A' + 10;
                            StartPositions[num] = new Point(x, y);
                            c = ' ';
                            break;
                        case 'O':
                        case 'P':
                        case 'Q':
                        case 'R':
                        case 'S':
                        case 'T':
                        case 'U':
                            item = GameItem.NewItem(c);
                            item.PosX = x;
                            item.PosY = y;
                            SecretTiles.Add(item);
                            c = ' ';
                            break;
                    }
                    item = GameItem.NewItem(c);
                    item.PosX = x;
                    item.PosY = y;
                    FixTiles[x, y] = item;
                }
            }
            DateTime end = DateTime.Now;
            for (int i = 0; i < PLAYERNUM; i++)
            {
                AddPlayer();
            }
            var handler = LevelReset;
            if (handler != null) handler(this, EventArgs.Empty);
        }
        public void StartTimers()
        {
            RenderTimer.Start();
            TimeCounterTimer.Start();
        }
        public void StopTimers()
        {
            RenderTimer.Stop();
            TimeCounterTimer.Stop();
        }

        void Level_Loaded(object sender, RoutedEventArgs e)
        {
            MyWin = Window.GetWindow(this);
            if (MyWin != null)
            {
                Reset();

                RenderTimer = new DispatcherTimer();
                RenderTimer.Interval = TimeSpan.FromMilliseconds(50);
                RenderTimer.Tick += Timer_Tick;

                TimeCounterTimer = new DispatcherTimer();
                TimeCounterTimer.Interval = TimeSpan.FromSeconds(1);
                TimeCounterTimer.Tick += (s, args) =>
                {
                    TimeLeft--;
                    if (TimeLeft <= 0)
                    {
                        SaveToFile();
                        Reset();
                    }
                };

                if (IsAutoPlay) StartTimers();

                InvalidateVisual();
            }
        }

        public void SaveToFile()
        {
            StringBuilder SB = new StringBuilder();
            foreach (Player akt in Players.OrderByDescending(x => x.TotalPoints).ToList())
            {
                SB.AppendLine(akt.ToString());
            }
            string now = String.Format("{0:D2}_{1:D2}_{2:D2}.tsv", DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            File.WriteAllText(now, SB.ToString());

        }

        public void SetPlayerNick(int num, string nick, BitmapImage img)
        {
            Players[num].NickName = nick;
            Players[num].Image = img;
            InvalidateVisual();
        }

        public void SetPlayerSpeed(int num, double speed)
        {
            if (!Players[num].IsActive) return;
            Players[num].WalkSpeed = speed;
        }
        public void SetPlayerBombNum(int num, int bombnum)
        {
            if (!Players[num].IsActive) return;
            Players[num].BombsAllowed = bombnum;
            Players[num].BombsLeft = bombnum;
        }
        public void SetPlayerBombSize(int num, int bombsize)
        {
            if (!Players[num].IsActive) return;
            Players[num].BombSize = bombsize;
        }

        public void ReversePlayer(int num)
        {
            if (!Players[num].IsActive) return;
            Players[num].DX = -Players[num].DX;
            Players[num].DY = -Players[num].DY;
        }
        public void MovePlayer(int num, int dx, int dy)
        {
            if (!Players[num].IsActive) return;
            if (Players[num].State == PlayerState.Standing)
            {
                Players[num].DX = dx;
                Players[num].DY = dy;
                Players[num].State = PlayerState.InTile;
                Players[num].MustStop = false;
            }
        }
        public void StopPlayer(int num)
        {
            if (!Players[num].IsActive) return;
            if (Players[num].State != PlayerState.Standing)
            {
                Players[num].MustStop = true;
            }
        }
        public void PlaceBomb(int num)
        {
            PlaceBomb(Players[num]);
        }
        private void PlaceBomb(Player p)
        {
            if (!p.IsActive) return;
            int posX = (int)Math.Round(p.PosX);
            int posY = (int)Math.Round(p.PosY);
            if (p.BombsLeft > 0 && p.NoBombStepsRemaining == 0)
            {
                Bomb b = (Bomb)GameItem.NewItem(Bomb.BOMBCHAR);
                b.PosX = posX;
                b.PosY = posY;
                b.TimeLeft = BOMBTIME;
                b.Owner = p;
                Bombs.Add(b);
                p.BombsLeft--;
            }
        }

        List<GameItem> SecretsThere(int NextX, int NextY)
        {
            return SecretTiles.Where(akt => akt.PosX == NextX && akt.PosY == NextY).ToList();
        }
        List<Bomb> FlamesThere(int NextX, int NextY)
        {
            return Flames.Where(akt => akt.PosX == NextX && akt.PosY == NextY).ToList();
        }
        bool IsBombThere(int NextX, int NextY)
        {
            return Bombs.Count(akt => akt.PosX == NextX && akt.PosY == NextY) > 0;
        }
        List<Player> PlayersThere(int X, int Y)
        {
            return Players.Where(akt => akt.PosX > X - 0.5 && akt.PosX < X + 0.5 &&
                                        akt.PosY > Y - 0.5 && akt.PosY < Y + 0.5).ToList();
        }

        bool IsInside(int x, int y)
        {
            return x >= 0 && x < FixTiles.GetLength(0) && y >= 0 && y < FixTiles.GetLength(1);
        }

        bool IsInTile(double value, double diff)
        {
            double target = Math.Round(value);
            double v1 = value - diff;
            double v2 = value;
            double v3 = value + diff;
            double diff1 = Math.Abs(target - v1);
            double diff2 = Math.Abs(target - v2);
            double diff3 = Math.Abs(target - v3);
            return diff1 > diff2 && diff2 <= diff3;
        }

        void Timer_Tick(object sender, EventArgs e)
        {
            TICKNUM++;

            #region Move & stop players
            foreach (Player aktPlayer in Players)
            {
                if (aktPlayer.State != PlayerState.Standing)
                {
                    if (aktPlayer.State == PlayerState.InTile)
                    {
                        int px = (int)Math.Round(aktPlayer.PosX);
                        int py = (int)Math.Round(aktPlayer.PosY);
                        aktPlayer.OneStep();
                        if (aktPlayer.AlwaysBombStepsRemaining > 0)
                        {
                            PlaceBomb(aktPlayer);
                        }

                        // Collect secrets
                        var secrets = SecretsThere(px, py);
                        if (secrets.Count > 0)
                        {
                            foreach (var aktSecret in secrets)
                            {
                                aktPlayer.SecretsCollected++;
                                switch (aktSecret.TileChar)
                                {
                                    case 'O': break;
                                    case 'P': aktPlayer.NoClipStepsRemaining = Player.NOCLIPSTEPS; break;
                                    case 'Q': aktPlayer.ArmorStepsRemaining = Player.SECRETSTEPS; break;
                                    case 'R': aktPlayer.SpeedUpStepsRemaining = Player.SECRETSTEPS; break;
                                    case 'S': aktPlayer.NoBombStepsRemaining = Player.SECRETSTEPS; break;
                                    case 'T': aktPlayer.SpeedDownStepsRemaining = Player.SECRETSTEPS; break;
                                    case 'U': aktPlayer.AlwaysBombStepsRemaining = Player.SECRETSTEPS; break;
                                }
                                SecretTiles.Remove(aktSecret);
                            }
                        }

                        // Stop or move to next tile
                        int NextX = px + aktPlayer.DX;
                        int NextY = py + aktPlayer.DY;
                        if (aktPlayer.MustStop ||
                            !IsInside(NextX, NextY) ||
                            (FixTiles[NextX, NextY].IsSolid && aktPlayer.NoClipStepsRemaining == 0) ||
                            (IsBombThere(NextX, NextY) && aktPlayer.NoClipStepsRemaining == 0))
                        {
                            aktPlayer.DX = 0; aktPlayer.DY = 0;
                            aktPlayer.State = PlayerState.Standing;
                        }
                        else
                        {
                            if (FixTiles[NextX, NextY].IsSolid || IsBombThere(NextX, NextY))
                            {
                                aktPlayer.NoClipStepsRemaining--;
                            }
                            aktPlayer.State = PlayerState.BetweenTiles;
                            aktPlayer.PosX += aktPlayer.CurrentWalkSpeed * aktPlayer.DX;
                            aktPlayer.PosY += aktPlayer.CurrentWalkSpeed * aktPlayer.DY;
                        }
                    }
                    else
                    {
                        double dx = aktPlayer.CurrentWalkSpeed * aktPlayer.DX;
                        double dy = aktPlayer.CurrentWalkSpeed * aktPlayer.DY;
                        aktPlayer.PosX += dx;
                        aktPlayer.PosY += dy;
                        //Console.WriteLine("X={0}; Speed={1}; Current={2}", aktPlayer.PosX, aktPlayer.WalkSpeed, aktPlayer.CurrentWalkSpeed);
                        //if (Math.Round(aktPlayer.PosX, 3) == Math.Round(aktPlayer.PosX) &&
                        //    Math.Round(aktPlayer.PosY, 3) == Math.Round(aktPlayer.PosY)) // egész koor.
                        if (IsInTile(aktPlayer.PosX, dx) || IsInTile(aktPlayer.PosY, dy))
                        {
                            aktPlayer.PosX = Math.Round(aktPlayer.PosX);
                            aktPlayer.PosY = Math.Round(aktPlayer.PosY);
                            aktPlayer.State = PlayerState.InTile;
                        }
                    }
                }
            }
            #endregion

            #region Tick bombs and flames
            List<Bomb> BombsToExplode = new List<Bomb>();
            foreach (Bomb akt in Bombs)
            {
                akt.TimeLeft--;
                if (akt.TimeLeft <= 0)
                {
                    BombsToExplode.Add(akt);
                }
            }

            List<Bomb> FlamesToRemove = new List<Bomb>();
            foreach (Bomb akt in Flames)
            {
                akt.TimeLeft--;
                if (akt.TimeLeft <= 0)
                {
                    FlamesToRemove.Add(akt);
                }
            }
            #endregion

            #region Explode bombs, place flames, remove walls
            for (int i = 0; i < BombsToExplode.Count; i++)
            {
                Bomb akt = BombsToExplode[i];
                Bombs.Remove(akt);
                akt.Owner.BombsDetonated++;
                akt.Owner.BombsLeft++;
                int posX = (int)akt.PosX;
                int posY = (int)akt.PosY;

                Bomb Flame = (Bomb)GameItem.NewItem(Bomb.FLAMECHAR);
                Flame.PosX = posX;
                Flame.PosY = posY;
                Flame.TimeLeft = BOMBTIME;
                Flame.Owner = akt.Owner;
                Flames.Add(Flame);

                for (int y = posY - 1; y >= posY - akt.Owner.BombSize; y--)
                {
                    if (PutFlame(akt, posX, y, BombsToExplode, Bombs)) break;
                }
                for (int y = posY + 1; y <= posY + akt.Owner.BombSize; y++)
                {
                    if (PutFlame(akt, posX, y, BombsToExplode, Bombs)) break;
                }
                for (int x = posX - 1; x >= posX - akt.Owner.BombSize; x--)
                {
                    if (PutFlame(akt, x, posY, BombsToExplode, Bombs)) break;
                }
                for (int x = posX + 1; x <= posX + akt.Owner.BombSize; x++)
                {
                    if (PutFlame(akt, x, posY, BombsToExplode, Bombs)) break;
                }
            }
            #endregion

            #region Kill players in flame
            foreach (Player aktPlayer in Players)
            {
                int posX = (int)Math.Round(aktPlayer.PosX);
                int posY = (int)Math.Round(aktPlayer.PosY);
                var flames = FlamesThere(posX, posY);
                if (flames.Count > 0)
                {
                    if (aktPlayer.IsActive && aktPlayer.ArmorStepsRemaining == 0)
                    {
                        aktPlayer.Died++;
                        if (aktPlayer.Died >= LIVES)
                        {
                            aktPlayer.IsActive = false;
                            aktPlayer.FullyDied++;
                        }

                        foreach (Bomb aktFlame in flames)
                        {
                            if (aktPlayer != aktFlame.Owner)
                            {
                                if (aktPlayer.Died >= LIVES) aktFlame.Owner.FullyKilled++; ;
                                //if (!aktPlayer.IsActive)
                                aktFlame.Owner.Kills++;
                            }
                            else
                            {
                                aktPlayer.SelfKills++;
                            }
                        }
                    }
                    FlamesToRemove.AddRange(flames);
                }
            }
            #endregion

            #region Kill players in flame - old
            /*
            foreach (Bomb aktFlame in Flames)
            {
                int posX = (int)aktFlame.PosX;
                int posY = (int)aktFlame.PosY;
                var players = PlayersThere(posX, posY);
                bool firstFlameInPosition = FlamesToRemove.Count(x => x.PosX == posX && x.PosY == posY) == 0;
                if (firstFlameInPosition)
                {
                    foreach (Player aktPlayer in players)
                    {
                        if (aktPlayer.IsActive && aktPlayer.ArmorStepsRemaining == 0)
                        {
                            aktPlayer.Died++;
                            if (aktPlayer.Died >= LIVES)
                            {
                                aktPlayer.IsActive = false;
                                aktPlayer.FullyDied++;
                                if (aktPlayer != aktFlame.Owner)
                                {
                                    aktFlame.Owner.FullyKilled++;
                                }
                            }
                            if (aktPlayer != aktFlame.Owner) aktFlame.Owner.Kills++;
                            if (aktPlayer == aktFlame.Owner) aktPlayer.SelfKills++;
                        }
                        else aktPlayer.ArmorStepsRemaining = 0;
                    }
                }
                if (players.Count > 0) FlamesToRemove.Add(aktFlame);
            }
            */
            #endregion

            // Remove flames
            foreach (Bomb akt in FlamesToRemove) { Flames.Remove(akt); }

            InvalidateVisual();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool PutFlame(Bomb akt, int posX, int posY, List<Bomb> BombsToExplode, List<Bomb> Bombs)
        {
            if (!IsInside(posX, posY)) return true;

            BombsToExplode.AddRange(Bombs.Where(x => x.PosX == posX && x.PosY == posY));
            foreach (Bomb aktBomb in BombsToExplode) Bombs.Remove(aktBomb);

            var flamesThre = FlamesThere(posX, posY);
            if (flamesThre.Count() != 0)
            {
                Bomb first = flamesThre.First();
                first.Owner = akt.Owner;
                first.TimeLeft = BOMBTIME;
                return false;
            }

            Bomb Flame = (Bomb)GameItem.NewItem(Bomb.FLAMECHAR);
            Flame.PosX = posX;
            Flame.PosY = posY;
            Flame.TimeLeft = BOMBTIME;
            Flame.Owner = akt.Owner;
            if (!FixTiles[posX, posY].IsSolid)
            {
                Flames.Add(Flame);
                return false;
            }
            else if (FixTiles[posX, posY].IsExplodable)
            {
                GameItem empty = GameItem.NewItem(' ');
                empty.PosX = posX;
                empty.PosY = posY;
                FixTiles[posX, posY] = empty;
                Flames.Add(Flame);
            }
            return true;
        }

        public string GetSerializedString()
        {
            StringBuilder SB = new StringBuilder();
            SB.Append("<GameItems objects=\"[[NUM]]\">");

            //SB.Append(Operations.Serialize(Players[0]));
            //return SB.ToString();

            foreach (Player akt in Players)
            {
                SB.Append(Operations.Serialize(akt));
            }
            foreach (GameItem akt in FixTiles)
            {
                SB.Append(Operations.Serialize(akt));
            }
            foreach (GameItem akt in SecretTiles)
            {
                SB.Append(Operations.Serialize(akt));
            }
            foreach (Bomb akt in Bombs)
            {
                SB.Append(Operations.Serialize(akt));
            }
            foreach (GameItem akt in Flames)
            {
                SB.Append(Operations.Serialize(akt));
            }
            TileNum = (FixTiles.Length + SecretTiles.Count + Players.Count + Bombs.Count + Flames.Count);
            SB.Replace("<?xml version=\"1.0\"?>", "");
            SB.Replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", "");
            SB.Replace("xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", "");
            SB.Replace("[[NUM]]", TileNum.ToString());
            SB.Append("</GameItems>");
            string str = SB.ToString();
            return str;
        }

        StringBuilder SB = new StringBuilder();
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (MyWin != null)
            {
                //SB.Clear();
                foreach (GameItem akt in FixTiles)
                {
                    akt.Render(drawingContext, TileSize);
                }
                foreach (GameItem akt in SecretTiles)
                {
                    akt.Render(drawingContext, TileSize);
                }
                foreach (Player akt in Players)
                {
                    akt.Render(drawingContext, TileSize);
                    //SB.Append(akt.ToString()).Append("\n");
                }
                foreach (Bomb akt in Bombs)
                {
                    akt.Render(drawingContext, TileSize);
                }
                foreach (GameItem akt in Flames)
                {
                    akt.Render(drawingContext, TileSize);
                }
                /*FormattedText formattedText = new FormattedText(
                    SB.ToString(),
                    System.Globalization.CultureInfo.CurrentCulture,
                    FlowDirection.LeftToRight,
                    TEXTFONT,
                    16,
                    Brushes.Black);
                drawingContext.DrawText(formattedText, TEXTLOC);*/
            }
        }
    }
}
