﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
/*
 * 20x20 -as pálya, 37x37 -es tile-ok
 * 
 * Alapszabály
 *      N játékos, M élet minden játékosnak, T ideig tart egy meccs, X mozgó (nem robbantgató) ellenfél => 
 *      max 5 perc egy meccs
 *      ha N=2, akkor aki életben marad, az nyer
 *      ha N>2, akkor annyiszor 100 pont, ahányadiknak kiesik
 *      
 * Pálya
 *      kezdőhelyek = 0..H a térképen, ugyanez a tilekép lesz a játékos képe (ők adják)
 *      
 *      V = robbantható fal
 *      W = fal
 *      X = bomba robbanás
 *      Y = bomba
 *      Z = space = üres
 * 
 * Extrák lehetnek (csak az aktuális mérkőzésre érvényes) :
 *      O = Kincs
 *      P = Átmehet bombán és falon X lépésig
 *      Q = Ideiglenes armor X lépésig  
 *      R = Ideiglenes gyorsulás X lépésig
 *      S = Nem rakhat le bombát X lépésig
 *      T = Ideiglenes lassulás X lépésig
 *      U = Folyamatos bomba lerakása X lépésig
 *      
 * Ha több marad életben a mérkőzés végén, akkor győztes eldöntése:
 *      felrobbantott ellenfelek száma?
 *      felrobbantott falak száma?
 *      lerakott bombák száma?
 *      lépések száma?
 *      kisfeladatokért kapott pontszám => kisfeladat megoldás 500 pont azért hogy jól megoldották, +500 pontot aki először adja le, -50p óránként (-500p max levonás)
 *      
 * Kisfeladatból jövő constant powerup
 *      mozgás +sebesség 1-5
 *      bomba tüzének mérete 1-5
 *      bombák száma 1-5
 *      Demo alatt távolról tudja beállítani az extrák erősségét
 *      
 * Játék menete:     
 *      a) kiinduló állapot: X játékos, a játéktéren a térkép szerint megadott helyen falak ÉS a szabad területen kincsek
 *      b) a játékosok elindulnak, robbantgatnak, pontot kapnak robbantásért + kincsért + másik játékos megöléséért; pontot veszítenek ha meghalnak
 *      c) a játék folyamán véletlenszerű kirobbantott fal időnként visszakerül a helyére (???)
 *      d) a játék folyamán véletlenszerű felszedett kincs időnként visszakerül a helyére (???)
 *      e) a játék során véletlenszerű nem kirobbantott falba "extra" kerül, ez a térképen látszódik, a játékos kirobbantás után felveheti, ha akarja
 *      
 * TODO
 *      OK X játékos mindig Y képpel játszon => Játékosoktól bekérni a képeket
 *      OK Kisfeladatok plusz extrái kerüljenek bele az INI-be
 *      OK GUI-ból indítható játék
 *      OK ha (isActive=false) akkor ne tudjon mozogni az ürgével
 *      OK Kliens duplikálás? (azonos név = azonos slot legyen!)
 *      OK 5 élet legyen, extra pont levonás/hozzáadás, ha az ötödiket veszti el/veszi el
 *      OK Mi van, ha két ember öl meg egy harmadikat???
 *      OK nickname hiányzik Reset() után
 *      OK Eredmény megjelenítse
 *      OK Client-ben lehessen debuggolni az UDP csomagokat
 *      OK? UDP comm áll másodpercekig
 *      OK Duplikálódó usernevek
 *      NO SecretTile -okat visszarakni
 *      OK minden játékos van minden pályán
 *      OK Minusz pontok ne legyenek
 *      OK Eredmény mentése file-ba
 *      
 *      OK Ha NickName-be rakok valami nem szokványos escape-szekvendiát, XML error
 *      
 * NEW TODO:
 *          IsActive=false játékos felrobbantása ne érjen pontot
 *          DLL-ek opcionális betöltése/indítása
 *          ???? bármennyi bombát lepakolhatok egyszerre ????
 */

using GeekDay4_Dyna.Classes;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using GeekDay4_Dyna.Game;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using DynaCommon;
using System.Runtime.CompilerServices;

namespace GeekDay4_Dyna
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string name = "")
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(name));
        }

        const bool DEMOCOMMANDS_ALLOWED = false;
        const bool AUTOSTART_GAME = true;
        //TaskScheduler GUIScheduler;

        public ObservableCollection<DynaDLLClient> Clients { get; private set; }
        public DynaDLLClient SelectedClient { get; set; }
        public int DesiredClientNum { get; set; }

        List<Player> players;
        public List<Player> Players
        {
            get { return players; }
            set { players = value; OnPropertyChanged(); }
        }

        void Assign(DynaDLLClient client)
        {
            for (int i = 0; i < DynaLevel.PlayerNum; i++)
            {
                if (Clients.Count(x => x.ClientNumber == i) == 0)
                {
                    client.ClientNumber = i;
                    DynaLevel.SetPlayerNick(i, client.NickName, client.Image);
                    return;
                }
            }
        }

        private void Assign_Click(object sender, RoutedEventArgs e)
        {
            if (SelectedClient != null)
            {
                if (DesiredClientNum <= 0)
                {
                    Assign(SelectedClient);
                }
                else
                {
                    SelectedClient.ClientNumber = DesiredClientNum;
                    DynaLevel.SetPlayerNick(DesiredClientNum, SelectedClient.NickName, SelectedClient.Image);
                }
            }
        }

        void LoadDLL()
        {
            string[] plugins = Directory.GetFiles(Environment.CurrentDirectory, "*.dll");
            foreach (string akt in plugins)
            {
                Assembly assembly = Assembly.LoadFile(akt);
                foreach (Type aktType in assembly.GetTypes())
                {
                    if (aktType.GetInterface("IDynaClient") != null)
                    {
                        //for (int i = 0; i < 18; i++)
                        {
                            IDynaClient instance = Activator.CreateInstance(aktType) as IDynaClient;
                            DynaDLLClient client = new DynaDLLClient();
                            client.CommandReceived += client_CommandReceived;
                            client.DLL = instance;
                            Clients.Add(client);
                            //Assign(client);
                        }
                    }
                }
            }
        }

        void client_CommandReceived(object sender, DynaCommand e)
        {
            Dispatcher.Invoke(() =>
            {
                int num = (sender as DynaDLLClient).ClientNumber;
                switch (e)
                {
                    case DynaCommand.CmdDown: DynaLevel.MovePlayer(num, 0, 1); break;
                    case DynaCommand.CmdUp: DynaLevel.MovePlayer(num, 0, -1); break;
                    case DynaCommand.CmdLeft: DynaLevel.MovePlayer(num, -1, 0); break;
                    case DynaCommand.CmdRight: DynaLevel.MovePlayer(num, 1, 0); break;
                    case DynaCommand.CmdStop: DynaLevel.StopPlayer(num); break;
                    case DynaCommand.CmdBomb: DynaLevel.PlaceBomb(num); break;
                    case DynaCommand.CmdReverse: DynaLevel.ReversePlayer(num); break;
                }
            }); //.Start(GUIScheduler);
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            //GUIScheduler = TaskScheduler.FromCurrentSynchronizationContext();
            Clients = new ObservableCollection<DynaDLLClient>();
            DynaLevel.IsAutoPlay = AUTOSTART_GAME;
            DynaLevel.LevelReset += DynaLevel_LevelReset;
            /*App.Current.DispatcherUnhandledException += (s, e) =>
            {
                // Handle the exception here
                MessageBox.Show(e.Exception.ToString());
            };*/

            DataContext = this;
            LoadDLL();

            DispatcherTimer refreshListviewTimer = new DispatcherTimer();
            refreshListviewTimer.Interval = TimeSpan.FromSeconds(1);
            refreshListviewTimer.Tick += (s, ex) =>
            {
                Players = null;
                Players = DynaLevel.Players;
            };
            refreshListviewTimer.Start();

            DispatcherTimer dataToClientsTimer = new DispatcherTimer();
            dataToClientsTimer.Interval = TimeSpan.FromMilliseconds(100);
            dataToClientsTimer.Tick += (s, ex) =>
            {
                string str = DynaLevel.GetSerializedString();
                this.Title = DynaLevel.TileNum.ToString();
                foreach (DynaDLLClient akt in Clients)
                {
                    akt.LevelXML = str;
                    akt.LevelTimeLeft = DynaLevel.TimeLeft;
                }

            };
            dataToClientsTimer.Start();

            DispatcherTimer autoSaveLevelTimer = new DispatcherTimer();
            autoSaveLevelTimer.Interval = TimeSpan.FromSeconds(5);
            autoSaveLevelTimer.Tick += (s, ex) =>
            {
                DynaLevel.SaveToFile();
            };
            autoSaveLevelTimer.Start();
        }

        public MainWindow()
        {
            InitializeComponent();
        }

        void DynaLevel_LevelReset(object sender, EventArgs e)
        {
            foreach (var akt in Clients)
            {
                if (akt.ClientNumber < 0)
                {
                    Assign(akt);
                }
                else
                {
                    DynaLevel.SetPlayerNick(akt.ClientNumber, akt.NickName, akt.Image);
                }
            }
        }

        private void Window_PreviewKeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                this.Close();

            int playerNum = SelectedClient != null ? SelectedClient.ClientNumber : -1;
            if (playerNum == -1) return;
            switch (e.Key)
            {
                case Key.S: DynaLevel.MovePlayer(playerNum, 0, 1); break;
                case Key.W: DynaLevel.MovePlayer(playerNum, 0, -1); break;
                case Key.A: DynaLevel.MovePlayer(playerNum, -1, 0); break;
                case Key.D: DynaLevel.MovePlayer(playerNum, 1, 0); break;
                case Key.LeftCtrl: DynaLevel.StopPlayer(playerNum); break;
                case Key.LeftShift: DynaLevel.PlaceBomb(playerNum); break;
                case Key.Y: DynaLevel.ReversePlayer(playerNum); break;
            }
        }

        private void StartStopClick(object sender, RoutedEventArgs e)
        {
            string tag = (sender as Button).Tag.ToString();
            if (tag == "0")
            {
                DynaLevel.StopTimers();
            }
            else
            {
                DynaLevel.StartTimers();
            }
        }

        private void ResetClick(object sender, RoutedEventArgs e)
        {
            DynaLevel.SaveToFile();
            DynaLevel.Reset();
        }

        private void SaveClick(object sender, RoutedEventArgs e)
        {
            DynaLevel.SaveToFile();
        }

        private void Window_Closing_1(object sender, CancelEventArgs e)
        {
            foreach (var akt in Clients)
            {
                akt.Abort();
            }
        }

    }
}
