﻿using System;
using DynaCommon;
using System.Diagnostics;

namespace ArgExClient
{
    public partial class ArgexAI
    {
        public static int CFC_CallCounter { get; private set; } //Says how many times CommandFromClient has been called when I had a map
        public static int TimeLeftUntilEnd { get; private set; }
        public static long CurrentServerRenderTicks { get; private set; }
        public static Map Map { get; private set; }
        public static bool HaveMap { get; private set; }

        public static readonly AI AI;
        public static DynaCommand previousCommand = DynaCommand.CmdStop;

        public DynaCommand CommandFromClientINNER(long numberOfServerTicks)
        {
            #region Error check and CallCounter++
            if (!HaveMap)
            {
                CurrentServerRenderTicks = numberOfServerTicks;
                return DynaCommand.CmdNothing; //Ignores calls of CommandFromClient WHILE we don't know anything of the map (first 4 calls atm)
            }
            CFC_CallCounter++;
            #endregion

            MapActualizer.Actualize(numberOfServerTicks - CurrentServerRenderTicks);
            CurrentServerRenderTicks = numberOfServerTicks;

            DynaCommand command = AI.GetNextCommand();

            #region NoBombStepsHANDLING     <may return>
            if (Map.MyPlayer.NoBombStepsRemaining > 0 && command == DynaCommand.CmdBomb)
            {
                //If I want to place a bomb while I can't, do a random but safe movement (NoBombSteps reduces at movement only)
                command = AI.RandomSafeMovement();
                AI.CurrentBehaviorCommand = AIBehavior.NoBombStepsHANDLING;

                goto LOG_AND_RETURN_THE_COMMAND;
            }
            #endregion
            #region Direction Change        <may return>
            if (((AI.IsMovingCommand(command) && AI.IsMovingCommand(previousCommand)) &&
                command != previousCommand))
            {
                AI.CurrentBehaviorCommand = AIBehavior.DirectionChange;
                command = DynaCommand.CmdStop;

                goto LOG_AND_RETURN_THE_COMMAND;
            }
            #endregion
            #region StopBeforeBomb          <may return>
            if (command == DynaCommand.CmdBomb && Map.MyPlayer.State != PlayerState.Standing)
            {
                AI.CurrentBehaviorCommand = AIBehavior.StopBeforeBomb;
                command = DynaCommand.CmdStop;

                goto LOG_AND_RETURN_THE_COMMAND;
            }
            #endregion

            // RETURNING:::
            LOG_AND_RETURN_THE_COMMAND:

            #region Only allow Up/Down/Left/Right when Standing     (CAUSES STUTTER)
            if (Map.MyPlayer.State == PlayerState.BetweenTiles &&
                command != DynaCommand.CmdStop &&
                command != DynaCommand.CmdReverse)
            {   //When I am Between Tiles, only allow Stop and Reverse
                AI.CurrentBehaviorCommand = AIBehavior.OnlyStopWhenBetweenTile;
                command = DynaCommand.CmdStop;
            }
            #endregion

            //Map.ErrorIfInvalidStep(command); //Check if I want to send a 'bullshit' command
            MapActualizer.ActualizeAfterCommand(command);
            previousCommand = command;
            if (command == DynaCommand.CmdBomb)
                AI.CallsUntilNextBomb = AI.CallsToWaitBetweenBombs;

            #region Log to Output
//#warning Log to Output STAYED IN CODE
            //string whoSaysWhat3 = string.Format("{0} says: {1}.", AI.CurrentBehaviorCommand, command.ToString()).PadRight(40);
            //string myX3 = string.Format("myX: {0}", Map.MyPlayer.PosX).PadRight(11);
            //string myY3 = "myY: " + Map.MyPlayer.PosY;
            //Console.WriteLine(whoSaysWhat3 + myX3 + myY3);
            #endregion

            return command;
        }

        static ArgexAI()
        {
            AI = new AI();
        }
    }
}