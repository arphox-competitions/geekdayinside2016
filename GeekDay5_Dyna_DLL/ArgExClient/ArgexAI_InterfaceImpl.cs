﻿using DynaCommon;
using System;
using System.Windows.Media.Imaging;

namespace ArgExClient
{
    public partial class ArgexAI : IDynaClient
    {
        public string NickName
        {
            get
            {
                //Logger.Log("EVENT: NickName Getter called");
                return Settings.General.ClientName;
            }
        }

        public BitmapImage TileOfClient()
        {
            //Use relative image paths, we will put custom images next to the EXE files
            //Logger.Log("EVENT: TileOfClient() called");
            return new BitmapImage(new Uri(Settings.General.ClientImageName, UriKind.Relative));
        }

        public void TimeLeft(int timeLeft)
        {
            //Logger.Log("EVENT: TimeLeft(int) called with parameter of " + timeLeft);
            TimeLeftUntilEnd = timeLeft;



            //File.WriteAllText(Logger.FileName,
            //    string.Format("CommandFromClient:\r\nAverage = {0} ms\r\nMax = {1} ms\r\nMin = {2} ms\r\n",
            //    PerformanceCounter.CommandFromClientTIMES.Average(),
            //    PerformanceCounter.CommandFromClientTIMES.Max(),
            //    PerformanceCounter.CommandFromClientTIMES.Min())
            //    );

            //File.AppendAllText(Logger.FileName,
            //    string.Format("\r\nItemsToClient:\r\nAverage = {0} ms\r\nMax = {1} ms\r\nMin = {2} ms",
            //    PerformanceCounter.ItemsToClientTIMES.Average(),
            //    PerformanceCounter.ItemsToClientTIMES.Max(),
            //    PerformanceCounter.ItemsToClientTIMES.Min())
            //    );

            //Environment.Exit(0);
        }

        public void ItemsToClient(string XMLData)
        {
            //Logger.Log("EVENT: ItemsToClient() called");

            //Stopwatch sw = Stopwatch.StartNew();
            #region Inside

//#warning IN RELEASE CODE UNCOMMENT ItemsToClient
            try
            {
                ItemsToClientINNER(XMLData);
            }
            catch (Exception e)
            {
                Logger.Log("ItemsToClient ERROR: " + e.Message);
            }

            #endregion

            //PerformanceCounter.CommandFromClientTIMES.Add(sw.ElapsedMilliseconds);
        }

        public DynaCommand CommandFromClient(long numberOfServerTicks)
        {
            //Logger.Log("EVENT: CommandFromClient() called");

            //Stopwatch sw = Stopwatch.StartNew();

            #region Inside

            DynaCommand command = DynaCommand.CmdNothing;
            //#warning IN RELEASE CODE UNCOMMENT CommandFromClient
            try
            {
                command = CommandFromClientINNER(numberOfServerTicks);
            }
            catch (Exception e)
            {
                Logger.Log("CommandFromClient ERROR: " + e.Message);
            }

            #endregion

            //PerformanceCounter.ItemsToClientTIMES.Add(sw.ElapsedMilliseconds);
            return command;
        }
    }
}