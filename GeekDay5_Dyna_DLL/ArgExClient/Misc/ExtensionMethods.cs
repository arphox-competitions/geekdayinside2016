﻿using System;
using System.Collections.Generic;

namespace ArgExClient
{
    public static class ExtensionMethods
    {
        public static bool HasBomb(this List<GameItem> source)
        {
            for (int i = 0; i < source.Count; i++)
                if (source[i].TileChar == 'Y')
                    return true;

            return false;
        }
        public static bool HasBombOrExplodableWall(this List<GameItem> source)
        {
            for (int i = 0; i < source.Count; i++)
                if (source[i].TileChar == 'Y' || source[i].TileChar == 'V')
                    return true;

            return false;
        }
        public static bool HasBombOrFlame(this List<GameItem> source)
        {
            for (int i = 0; i < source.Count; i++)
                if (source[i].TileChar == 'X' || source[i].TileChar == 'Y')
                    return true;

            return false;
        }
        public static bool HasBombOrWall(this List<GameItem> source)
        {
            foreach (var item in source)
            {
                if (item is Wall || item.TileChar == 'Y')
                {
                    return true;
                }
            }
            return false;
        }
        public static bool HasEnemyThere(this List<Player> source, int TilePosX, int TilePosY)
        {
            foreach (Player item in source)
            {
                if(item.TilePosX == TilePosX && item.TilePosY == TilePosY)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool HasExplodableWall(this List<GameItem> source)
        {
            for (int i = 0; i < source.Count; i++)
                if (source[i].TileChar == 'V')
                    return true;

            return false;
        }
        public static bool HasFlame(this List<GameItem> source)
        {
            for (int i = 0; i < source.Count; i++)
                if (source[i].TileChar == 'X')
                    return true;

            return false;
        }
        public static bool HasUndestroyableWall(this List<GameItem> source)
        {
            for (int i = 0; i < source.Count; i++)
                if (source[i].TileChar == 'W')
                    return true;

            return false;
        }
        public static bool HasWall(this List<GameItem> source)
        {
            for (int i = 0; i < source.Count; i++)
                if (source[i] is Wall)
                    return true;

            return false;
        }
        public static Player FindMyself(this List<Player> source)
        {
            for (int i = 0; i < source.Count; i++)
            {
                if (source[i].NickName == Settings.General.ClientName)
                {
                    return source[i];
                }
            }

            throw new ApplicationException("I haven't find myself in the players' list!");
        }
    }
}