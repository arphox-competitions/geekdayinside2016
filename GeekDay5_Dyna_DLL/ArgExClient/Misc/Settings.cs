﻿namespace ArgExClient
{
    public static class Settings
    {
        public static class General
        {
            public const string ClientName = "ArgEx";
            public const string ClientImageName = "argex.png";
            //public const string LogDirectory = @"c:\argex\Log\"; //With '\' at the end!
            public const string LogDirectory = "ArgEx_LOG "; //With '\' at the end!
            public static readonly bool LogActive = true;

            public static readonly int MapSizeX = 20;
            public static readonly int MapSizeY = 20;

            //Number of placable bombs at once is 3 and WON'T CHANGE, according to the Game Master
        }
        public static class FromServer
        {
            public static class LEVEL
            {
                //public const int GAMETIME = 30;
                //public const int PLAYERNUM = 18;
                public const int BOMBTIME = 30;
                //public const int LIVES = 100;
            }
            public static class PLAYER
            {
                //public const double INITWALKSPEED = 0.25;
                //public const int INITBOMBSALLOWED = 3;
                //public const int INITBOMBSIZE = 2;

                public const string DEFAULTNAME = "_NA_";
                public const int NOCLIPSTEPS = 2;
                public const int SECRETSTEPS = 20;
                public const double SPEEDFACTOR = 0.5;
            }
        }

        public static class Special
        {
            public const int VirtualBombID = -66666666;
        }
    }
}