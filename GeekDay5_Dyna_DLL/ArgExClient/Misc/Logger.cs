﻿using System;
using System.IO;

namespace ArgExClient
{
    public static class Logger
    {
        public static string FileName;

        public static void LogWithoutSpace(string message)
        {
            if (!Settings.General.LogActive)
                return;

            File.AppendAllText(FileName, DateTime.Now.ToString() + "." + DateTime.Now.Millisecond.ToString("000") + " " + message);
        }
        public static void Log(string message)
        {
            LogWithoutSpace(message + "\n");
        }
        public static void LogMap(GameItemCollection[,] map)
        {
            if (!Settings.General.LogActive)
                return;

            string file = string.Empty;
            for (int y = 0; y < map.GetLength(1); y++)
            {
                for (int x = 0; x < map.GetLength(0); x++)
                {
                    file += map[x, y].GetMapChar();
                }
                file += '\n';
            }
            File.WriteAllText(Settings.General.LogDirectory + "map.txt", file);
        }
        public static void LogIntMap(int[,] map)
        {
            if (!Settings.General.LogActive)
                return;

            string file = string.Empty;
            for (int y = 0; y < map.GetLength(1); y++)
            {
                for (int x = 0; x < map.GetLength(0); x++)
                {
                    file += String.Format("{0,3}", map[x,y]);
                }
                file += '\n';
            }
            File.WriteAllText(Settings.General.LogDirectory + "hullamMAP.txt", file);
        }

        static Logger()
        {
            FileName = Settings.General.LogDirectory;
            FileName += string.Format("{0}.{1}.{2}. {3}-{4}-{5}",
                DateTime.Today.Date.Year.ToString("00"), //00 = With Leading Zero (e.g. March => "03" and not "3")
                DateTime.Today.Date.Month.ToString("00"),
                DateTime.Today.Date.Day.ToString("00"),
                DateTime.Now.Hour.ToString("00"),
                DateTime.Now.Minute.ToString("00"),
                DateTime.Now.Second.ToString("00"))
                + ".txt";
        }
    }
}