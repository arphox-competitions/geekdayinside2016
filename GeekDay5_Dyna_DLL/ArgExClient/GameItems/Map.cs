﻿using System.Collections.Generic;
using System.Linq;
using DynaCommon;
using System;

namespace ArgExClient
{
    public class Map
    {
        public enum DangerDirection
        {
            NoDanger, InPlace, Left, Right, Upwards, Downwards
        }

        public GameItemCollection[,] Items { get; private set; }
        public GameItemCollection this[int x, int y]
        {
            get { return Items[x, y]; }
        }

        public List<Player> Enemies;
        public List<PowerUp> PowerUps;
        public List<Bomb> Bombs;
        public List<Flame> Flames;
        public Player MyPlayer;

        public Map()
        {
            Items = new GameItemCollection[Settings.General.MapSizeX, Settings.General.MapSizeY];
            for (int i = 0; i < Items.GetLength(0); i++)
                for (int j = 0; j < Items.GetLength(1); j++)
                    Items[i, j] = new GameItemCollection();

            Enemies = new List<Player>();
            PowerUps = new List<PowerUp>();
            Bombs = new List<Bomb>();
            Flames = new List<Flame>();
        }

        public DangerDirection BombHarming(int column, int row)
        {
            if (Items[column, row].Items.HasBomb())        //TILE
                return DangerDirection.InPlace;

            if (column + 1 < Settings.General.MapSizeX &&
                Items[column + 1, row].Items.HasBomb())    //RIGHT 1
                return DangerDirection.Right;

            if (column + 2 < Settings.General.MapSizeX &&
                !Items[column + 1, row].Items.HasWall() &&
                Items[column + 2, row].Items.HasBomb())    //RIGHT 2
                return DangerDirection.Right;

            if (row + 1 < Settings.General.MapSizeY &&
                Items[column, row + 1].Items.HasBomb())    //DOWN 1
                return DangerDirection.Downwards;

            if (row + 2 < Settings.General.MapSizeY &&
                !Items[column, row + 1].Items.HasWall() &&
                Items[column, row + 2].Items.HasBomb())    //DOWN 2
                return DangerDirection.Downwards;

            if (column > 0 &&
                Items[column - 1, row].Items.HasBomb())    //LEFT 1
                return DangerDirection.Left;

            if (column > 1 &&
                !Items[column - 1, row].Items.HasWall() &&
                Items[column - 2, row].Items.HasBomb())    //LEFT 2
                return DangerDirection.Left;

            if (row > 0 &&
                Items[column, row - 1].Items.HasBomb())    //UP 1
                return DangerDirection.Upwards;

            if (row > 1 &&
                !Items[column, row - 1].Items.HasWall() &&
                Items[column, row - 2].Items.HasBomb())    //UP 2
                return DangerDirection.Upwards;

            return DangerDirection.NoDanger;
        }
        public DangerDirection BombHarmingWithMyDirection(DynaCommand direction, int column, int row)
        {
            //IGNORES THE DANGER FROM OUR DIRECTION
            switch (direction)
            {
                #region Down
                case DynaCommand.CmdDown:
                    if (column + 1 < Settings.General.MapSizeX &&
                        Items[column + 1, row].Items.HasBomb())    //RIGHT 1
                        return DangerDirection.Right;

                    if (column + 2 < Settings.General.MapSizeX &&
                        !Items[column + 1, row].Items.HasWall() &&
                        Items[column + 2, row].Items.HasBomb())    //RIGHT 2
                        return DangerDirection.Right;

                    if (row + 1 < Settings.General.MapSizeY &&
                        Items[column, row + 1].Items.HasBomb())    //DOWN 1
                        return DangerDirection.Downwards;

                    if (row + 2 < Settings.General.MapSizeY &&
                        !Items[column, row + 1].Items.HasWall() &&
                        Items[column, row + 2].Items.HasBomb())    //DOWN 2
                        return DangerDirection.Downwards;

                    if (column > 0 &&
                        Items[column - 1, row].Items.HasBomb())    //LEFT 1
                        return DangerDirection.Left;

                    if (column > 1 &&
                        !Items[column - 1, row].Items.HasWall() &&
                        Items[column - 2, row].Items.HasBomb())    //LEFT 2
                        return DangerDirection.Left;

                    break;
                #endregion
                #region Up
                case DynaCommand.CmdUp:
                    if (column + 1 < Settings.General.MapSizeX &&
                        Items[column + 1, row].Items.HasBomb())    //RIGHT 1
                        return DangerDirection.Right;

                    if (column + 2 < Settings.General.MapSizeX &&
                        !Items[column + 1, row].Items.HasWall() &&
                        Items[column + 2, row].Items.HasBomb())    //RIGHT 2
                        return DangerDirection.Right;

                    if (column > 0 &&
                        Items[column - 1, row].Items.HasBomb())    //LEFT 1
                        return DangerDirection.Left;

                    if (column > 1 &&
                        !Items[column - 1, row].Items.HasWall() &&
                        Items[column - 2, row].Items.HasBomb())    //LEFT 2
                        return DangerDirection.Left;

                    if (row > 0 &&
                        Items[column, row - 1].Items.HasBomb())    //UP 1
                        return DangerDirection.Upwards;

                    if (row > 1 &&
                        !Items[column, row - 1].Items.HasWall() &&
                        Items[column, row - 2].Items.HasBomb())    //UP 2
                        return DangerDirection.Upwards;

                    break;
                #endregion
                #region Left
                case DynaCommand.CmdLeft:
                    if (row + 1 < Settings.General.MapSizeY &&
                        Items[column, row + 1].Items.HasBomb())    //DOWN 1
                        return DangerDirection.Downwards;

                    if (row + 2 < Settings.General.MapSizeY &&
                        !Items[column, row + 1].Items.HasWall() &&
                        Items[column, row + 2].Items.HasBomb())    //DOWN 2
                        return DangerDirection.Downwards;

                    if (column > 0 &&
                        Items[column - 1, row].Items.HasBomb())    //LEFT 1
                        return DangerDirection.Left;

                    if (column > 1 &&
                        !Items[column - 1, row].Items.HasWall() &&
                        Items[column - 2, row].Items.HasBomb())    //LEFT 2
                        return DangerDirection.Left;

                    if (row > 0 &&
                        Items[column, row - 1].Items.HasBomb())    //UP 1
                        return DangerDirection.Upwards;

                    if (row > 1 &&
                        !Items[column, row - 1].Items.HasWall() &&
                        Items[column, row - 2].Items.HasBomb())    //UP 2
                        return DangerDirection.Upwards;

                    break;
                #endregion
                #region Right
                case DynaCommand.CmdRight:
                    if (column + 1 < Settings.General.MapSizeX &&
                        Items[column + 1, row].Items.HasBomb())    //RIGHT 1
                        return DangerDirection.Right;

                    if (column + 2 < Settings.General.MapSizeX &&
                        !Items[column + 1, row].Items.HasWall() &&
                        Items[column + 2, row].Items.HasBomb())    //RIGHT 2
                        return DangerDirection.Right;

                    if (row + 1 < Settings.General.MapSizeY &&
                        Items[column, row + 1].Items.HasBomb())    //DOWN 1
                        return DangerDirection.Downwards;

                    if (row + 2 < Settings.General.MapSizeY &&
                        !Items[column, row + 1].Items.HasWall() &&
                        Items[column, row + 2].Items.HasBomb())    //DOWN 2
                        return DangerDirection.Downwards;

                    if (row > 0 &&
                        Items[column, row - 1].Items.HasBomb())    //UP 1
                        return DangerDirection.Upwards;

                    if (row > 1 &&
                        !Items[column, row - 1].Items.HasWall() &&
                        Items[column, row - 2].Items.HasBomb())    //UP 2
                        return DangerDirection.Upwards;
                    break;
                    #endregion
            }

            return DangerDirection.NoDanger;
        }

        public bool DeadEndThere(DynaCommand direction, int fromX, int fromY)
        {
            switch (direction)
            {
                #region Left
                case DynaCommand.CmdLeft:
                    {
                        //  #       ##
                        // # B     #  B
                        //  #       ##
                        if ((
                            fromX > 1 && fromY > 0 && fromY + 1 < Settings.General.MapSizeY &&
                            this[fromX - 1, fromY - 1].CanWalkThrough == false &&   //I can't go LEFT UP   and
                            this[fromX - 1, fromY + 1].CanWalkThrough == false &&   //I can't go LEFT DOWN and
                            this[fromX - 2, fromY].CanWalkThrough == false &&       //I can't go LEFT LEFT and
                            this[fromX - 1, fromY].CanWalkThrough == true           //I can go LEFT
                            )
                            ||                                                  //OR
                            (
                            fromX > 2 && fromY > 0 && fromY + 1 < Settings.General.MapSizeY &&
                            this[fromX - 1, fromY - 1].CanWalkThrough == false &&   //I can't go LEFT UP        and
                            this[fromX - 2, fromY - 1].CanWalkThrough == false &&   //I can't go LEFT LEFT UP   and
                            this[fromX - 1, fromY + 1].CanWalkThrough == false &&   //I can't go LEFT DOWN      and
                            this[fromX - 2, fromY + 1].CanWalkThrough == false &&   //I can't go LEFT LEFT DOWN and
                            this[fromX - 3, fromY].CanWalkThrough == false &&       //I can't go LEFT LEFT LEFT and
                            this[fromX - 1, fromY].CanWalkThrough == true &&        //I can go LEFT         and
                            this[fromX - 2, fromY].CanWalkThrough == true           //I can go LEFT LEFT
                            )
                            )
                        {
                            return true;
                        }
                        else
                            return false;
                    }
                #endregion
                #region Right
                case DynaCommand.CmdRight:
                    {
                        //  #       ##
                        // B #     B  #
                        //  #       ##

                        if ((
                            fromX + 2 < Settings.General.MapSizeX && fromY > 0 && fromY + 1 < Settings.General.MapSizeY &&
                            this[fromX + 1, fromY - 1].CanWalkThrough == false &&   //I can't go RIGHT UP    and
                            this[fromX + 1, fromY + 1].CanWalkThrough == false &&   //I can't go RIGHT DOWN  and
                            this[fromX + 2, fromY].CanWalkThrough == false &&       //I can't go RIGHT RIGHT and
                            this[fromX + 1, fromY].CanWalkThrough == true               //I can go RIGHT
                            )
                            ||                                                  //OR
                            (
                            fromX + 3 < Settings.General.MapSizeX && fromY > 0 && fromY + 1 < Settings.General.MapSizeY &&
                            this[fromX + 1, fromY - 1].CanWalkThrough == false &&   //I can't go RIGHT UP          and
                            this[fromX + 2, fromY - 1].CanWalkThrough == false &&   //I can't go RIGHT RIGHT UP    and
                            this[fromX + 1, fromY + 1].CanWalkThrough == false &&   //I can't go RIGHT DOWN        and
                            this[fromX + 2, fromY + 1].CanWalkThrough == false &&   //I can't go RIGHT RIGHT DOWN  and
                            this[fromX + 3, fromY].CanWalkThrough == false &&       //I can't go RIGHT RIGHT RIGHT and
                            this[fromX + 1, fromY].CanWalkThrough == true &&            //I can go RIGHT           and
                            this[fromX + 2, fromY].CanWalkThrough == true               //I can go RIGHT RIGHT
                            )
                            )
                        {
                            return true;
                        }
                        else
                            return false;
                    }
                #endregion
                #region Up
                case DynaCommand.CmdUp:
                    {
                        //  #       #
                        // # #     # #
                        //  B      # #
                        //          B        

                        if ((
                            fromY > 1 && fromX > 0 && fromX + 1 < Settings.General.MapSizeX &&
                            this[fromX + 1, fromY - 1].CanWalkThrough == false &&   //I can't go UP RIGHT       and
                            this[fromX - 1, fromY - 1].CanWalkThrough == false &&   //I can't go UP LEFT        and
                            this[fromX, fromY - 2].CanWalkThrough == false &&       //I can't go UP UP          and
                            this[fromX, fromY - 1].CanWalkThrough == true               //I can go UP
                            )
                            ||                                                  //OR
                            (
                            fromY > 2 && fromX > 0 && fromX + 1 < Settings.General.MapSizeX &&
                            this[fromX + 1, fromY - 1].CanWalkThrough == false &&   //I can't go UP RIGHT       and
                            this[fromX + 1, fromY - 2].CanWalkThrough == false &&   //I can't go UP UP RIGHT    and
                            this[fromX - 1, fromY - 1].CanWalkThrough == false &&   //I can't go UP LEFT        and
                            this[fromX - 1, fromY - 2].CanWalkThrough == false &&   //I can't go UP UP LEFT     and
                            this[fromX, fromY - 3].CanWalkThrough == false &&       //I can't go UP UP UP       and
                            this[fromX, fromY - 2].CanWalkThrough == true &&            //I can go UP UP        and
                            this[fromX, fromY - 1].CanWalkThrough == true               //I can go UP
                            )
                            )
                        {
                            return true;
                        }
                        else
                            return false;
                    }
                #endregion
                #region Down
                case DynaCommand.CmdDown:
                    {
                        //  B       B
                        // # #     # #
                        //  #      # #
                        //          #        

                        if ((
                            fromY + 2 < Settings.General.MapSizeY && fromX > 0 && fromX + 1 < Settings.General.MapSizeX &&
                            this[fromX + 1, fromY + 1].CanWalkThrough == false &&   //I can't go DOWN RIGHT         and
                            this[fromX - 1, fromY + 1].CanWalkThrough == false &&   //I can't go DOWN LEFT          and
                            this[fromX, fromY + 2].CanWalkThrough == false &&       //I can't go DOWN DOWN          and
                            this[fromX, fromY + 1].CanWalkThrough == true               //I can go DOWN
                            )
                            ||                                                  //OR
                            (
                            fromY + 3 < Settings.General.MapSizeY && fromX > 0 && fromX + 1 < Settings.General.MapSizeX &&
                            this[fromX + 1, fromY + 1].CanWalkThrough == false &&   //I can't go DOWN RIGHT         and
                            this[fromX + 1, fromY + 2].CanWalkThrough == false &&   //I can't go DOWN DOWN RIGHT    and
                            this[fromX - 1, fromY + 1].CanWalkThrough == false &&   //I can't go DOWN LEFT          and
                            this[fromX - 1, fromY + 2].CanWalkThrough == false &&   //I can't go DOWN DOWN LEFT     and
                            this[fromX, fromY + 3].CanWalkThrough == false &&       //I can't go DOWN DOWN DOWN     and
                            this[fromX, fromY + 2].CanWalkThrough == true &&            //I can go DOWN DOWN        and
                            this[fromX, fromY + 1].CanWalkThrough == true               //I can go DOWN
                            )
                            )
                        {
                            return true;
                        }
                        else
                            return false;
                    }
                    #endregion
            }

            return false;
        }


        public static bool IsInsideTheMap(int x, int y)
        {
            return x >= 0 && x < Settings.General.MapSizeX && y >= 0 && y < Settings.General.MapSizeY;
        }
        public bool MyPlayerCanGoThere(DynaCommand direction)
        {
            int myX = MyPlayer.TilePosX;
            int myY = MyPlayer.TilePosY;

            switch (direction)
            {
                case DynaCommand.CmdLeft: return (myX > 0 && Items[myX - 1, myY].CanWalkThrough);
                case DynaCommand.CmdRight: return (myX + 1 < Settings.General.MapSizeX && Items[myX + 1, myY].CanWalkThrough);
                case DynaCommand.CmdUp: return (myY > 0 && Items[myX, myY - 1].CanWalkThrough);
                case DynaCommand.CmdDown: return (myY + 1 < Settings.General.MapSizeY && Items[myX, myY + 1].CanWalkThrough);

                case DynaCommand.CmdStop: return true;
                case DynaCommand.CmdReverse: return true;
                case DynaCommand.CmdNothing: return true;
            }

            throw new ApplicationException("Unexpected direction at CanGoThere(DynaCommand direction)");
        }
        public bool DangerousToGoThere(DynaCommand direction)
        {
            if (ArgexAI.Map.MyPlayer.ArmorStepsRemaining > 1) return false; //If I have armor, I'm a fucking hero

            int myX = ArgexAI.Map.MyPlayer.TilePosX;
            int myY = ArgexAI.Map.MyPlayer.TilePosY;

            switch (direction)
            {
                case DynaCommand.CmdDown:
                    if (myY < Settings.General.MapSizeY -1 &&
                        (ArgexAI.Map.Items[myX, myY + 1].Items.HasBombOrFlame() ||
                        ArgexAI.Map.BombHarmingWithMyDirection(direction, myX, myY + 1) != DangerDirection.NoDanger))
                    {
                        return true;
                    }
                    break;
                case DynaCommand.CmdUp:
                    if (myY > 0 &&
                        (ArgexAI.Map.Items[myX, myY - 1].Items.HasBombOrFlame() ||
                        ArgexAI.Map.BombHarmingWithMyDirection(direction, myX, myY - 1) != DangerDirection.NoDanger))
                    {
                        return true;
                    }
                    break;
                case DynaCommand.CmdLeft:
                    if (myX > 0 && 
                        (ArgexAI.Map.Items[myX - 1, myY].Items.HasBombOrFlame() ||
                        ArgexAI.Map.BombHarmingWithMyDirection(direction, myX - 1, myY) != DangerDirection.NoDanger))
                    {
                        return true;
                    }
                    break;
                case DynaCommand.CmdRight:
                    if (myX < Settings.General.MapSizeX -1 &&
                        (ArgexAI.Map.Items[myX + 1, myY].Items.HasBombOrFlame() ||
                        ArgexAI.Map.BombHarmingWithMyDirection(direction, myX + 1, myY) != DangerDirection.NoDanger))
                    {
                        return true;
                    }
                    break;

            }
            return false;
        }
        public void ErrorIfInvalidStep(DynaCommand command)
        {
            int myX = MyPlayer.TilePosX;
            int myY = MyPlayer.TilePosY;

            switch (command)
            {
                case DynaCommand.CmdLeft:
                    if (!Items[myX - 1, myY].CanWalkThrough && Items[myX, myX].CanWalkThrough)
                        throw new ApplicationException("(For some stupid reason) I want to move LEFT, but I can't (according to the map)!");
                    break;
                case DynaCommand.CmdRight:
                    if (!Items[myX + 1, myY].CanWalkThrough && Items[myX, myX].CanWalkThrough)
                        throw new ApplicationException("(For some stupid reason) I want to move RIGHT, but I can't (according to the map)!");
                    break;
                case DynaCommand.CmdUp:
                    if (!Items[myX, myY - 1].CanWalkThrough && Items[myX, myX].CanWalkThrough)
                        throw new ApplicationException("(For some stupid reason) I want to move UP, but I can't (according to the map)!");
                    break;
                case DynaCommand.CmdDown:
                    if (!Items[myX, myY + 1].CanWalkThrough && Items[myX, myX].CanWalkThrough)
                        throw new ApplicationException("(For some stupid reason) I want to move DOWN, but I can't (according to the map)!");
                    break;
            }
        }
        public DynaCommand RemoveInvalidStep(DynaCommand command)
        {
            int myX = MyPlayer.TilePosX;
            int myY = MyPlayer.TilePosY;
            int newX = myX;
            int newY = myY;

            switch (command)
            {
                case DynaCommand.CmdLeft: newX = myX - 1; break;
                case DynaCommand.CmdRight: newX = myX + 1; break;
                case DynaCommand.CmdUp: newY = myY - 1; break;
                case DynaCommand.CmdDown: newY = myY + 1; break;
            }

            if (!MapActualizer.IsInsideTheMap(newX, newY))
                return DynaCommand.CmdNothing;

            if (!Items[newX, newY].CanWalkThrough)
                return DynaCommand.CmdNothing;

            return command;
        }

        public bool ActiveEnemyInBombRangeThere(int PosX, int PosY)
        {
            //Says that if I place a bomb at PosY:PosY, I may kill a nearby enemy

            List<Player> enemies = ArgexAI.Map.Enemies.Where(   //Active and vulnerable enemies
                s => s.IsActive == true &&
                s.NickName != Settings.FromServer.PLAYER.DEFAULTNAME &&
                s.ArmorStepsRemaining <= 0
                ).ToList();

            if (enemies.HasEnemyThere(PosX, PosY)) return true;       //IN PLACE

            #region Right
            if (PosX + 1 < Settings.General.MapSizeX && !ArgexAI.Map[PosX + 1, PosY].Items.HasBombOrWall())
            {
                if (enemies.HasEnemyThere(PosX + 1, PosY)) return true;

                if (PosX + 2 < Settings.General.MapSizeX &&
                    !ArgexAI.Map[PosX + 2, PosY].Items.HasBombOrWall() &&
                    enemies.HasEnemyThere(PosX + 2, PosY))
                {
                    return true;
                }
            }
            #endregion
            #region Left
            if (PosX > 0 && !ArgexAI.Map[PosX - 1, PosY].Items.HasBombOrWall())
            {
                if (enemies.HasEnemyThere(PosX - 1, PosY)) return true;

                if (PosX > 1 &&
                    !ArgexAI.Map[PosX - 2, PosY].Items.HasBombOrWall() &&
                    enemies.HasEnemyThere(PosX - 2, PosY))
                {
                    return true;
                }
            }
            #endregion
            #region Up
            if (PosY > 0 && !ArgexAI.Map[PosX, PosY - 1].Items.HasBombOrWall())
            {
                if (enemies.HasEnemyThere(PosX, PosY - 1)) return true;

                if (PosY > 1 &&
                    !ArgexAI.Map[PosX, PosY - 2].Items.HasBombOrWall() &&
                    enemies.HasEnemyThere(PosX, PosY - 2))
                {
                    return true;
                }
            }
            #endregion
            #region Down
            if (PosY + 1 < Settings.General.MapSizeY && !ArgexAI.Map[PosX, PosY + 1].Items.HasBombOrWall())
            {
                if (enemies.HasEnemyThere(PosX, PosY + 1)) return true;

                if (PosY + 2 < Settings.General.MapSizeY &&
                    !ArgexAI.Map[PosX, PosY + 2].Items.HasBombOrWall() &&
                    enemies.HasEnemyThere(PosX, PosY + 2))
                {
                    return true;
                }
            }
            #endregion

            return false;
        }
    }
}