﻿using System.Linq;
using System.Collections.Generic;

namespace ArgExClient
{
    public class GameItemCollection
    {
        public List<GameItem> Items { get; set; }
        bool itemsReady = false;
        public bool ItemsReady
        {
            get { return itemsReady; }
            set
            {
                itemsReady = true;
                CalcWalkthrough();
            }
        }

        private void CalcWalkthrough()
        {
            foreach (GameItem item in Items)
            {
                if (item is Wall || //DESTROYABLE WALL TOO!!!
                    item.TileChar == 'Y')
                {
                    CanWalkThrough = false;
                    return;
                }
            }
            CanWalkThrough = true;
        }
        private bool canWalkThrough;
        public bool CanWalkThrough
        {
            get
            {
                if (ArgexAI.Map.MyPlayer.NoClipStepsRemaining > 1)
                    return true;
                else
                    return canWalkThrough;
            }
            set { canWalkThrough = value; }
        }

        public GameItemCollection()
        {
            Items = new List<GameItem>();
        }

        /// <summary>
        /// Returns the character for the "place"
        /// </summary>
        public char GetMapChar()
        {
            //Only used in map logging
            if (Items.Count == 0)
                return '-';
            if (Items.Count == 1)
            {
                if (Items[0].TileChar == 'Z')
                    return ' ';
                else
                    return Items[0].TileChar;
            }
            //Count > 1
            return Items.Where(x => !(x.TileChar == 'Z')).First().TileChar;
        }

        public override string ToString()
        {
            return string.Format("X:{2} Y:{3}, {0} items, CanWalkThrough = {1}", Items.Count, CanWalkThrough, Items[0].PosX, Items[0].PosY);
        }
    }
}