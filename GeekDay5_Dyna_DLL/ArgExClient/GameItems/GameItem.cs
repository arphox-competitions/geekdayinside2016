﻿using System;
using System.Globalization;
using System.Xml.Linq;

namespace ArgExClient
{
    public class GameItem
    {
        //When performance is critical, I think public fields are a bit faster than auto-properties so I used them,
        //so for the variables that are called a lot, I changed them to public fields.

        public int ID { get; set; }

        public char TileChar;
        //public bool IsExplodable { get; set; }
        //public bool IsSolid { get; set; }

        public double PosX;
        public double PosY;
        public string NickName { get; set; }

        //NEW (by. Arphox)
        public int DistanceFromUs { get; set; }

        public GameItem()
        {

        }
        public GameItem(XElement element)
        {
            ID = int.Parse(element.Element("ID").Value);
            TileChar = Convert.ToChar(int.Parse(element.Element("TileChar").Value));
            //IsExplodable = bool.Parse(element.Element("IsExplodable").Value);
            //IsSolid = bool.Parse(element.Element("IsSolid").Value);
            PosX = double.Parse(element.Element("PosX").Value, CultureInfo.InvariantCulture);
            PosY = double.Parse(element.Element("PosY").Value, CultureInfo.InvariantCulture);
            NickName = element.Element("NickName").Value;
            DistanceFromUs = int.MaxValue;
        }

        public override string ToString()
        {
            return string.Format("{0}: [{1};{2}] ({3})", TileChar, PosX, PosY, ID);
        }
    }
}