﻿using System;
using System.Collections.Generic;
using DynaCommon;
using System.Linq;
using System.Diagnostics;

namespace ArgExClient
{
    public class AI
    {
        //BEHAVIOR:
        public const int CallsToWaitBetweenBombs = 30; //REDUCE IT VERY CAREFULLY!

        //Map                       = ArgexAI.Map
        //CurrentServerRenderTicks  = ArgexAI.CurrentServerRenderTicks
        //HaveMap                   = ArgexAI.HaveMap
        //TimeLeftUntilEnd          = ArgexAI.TimeLeftUntilEndTimeLeftUntilEnd

        public static AIBehavior CurrentBehaviorCommand = AIBehavior.INITIAL;   //DEBUG feature

        public static int CallsUntilNextBomb = 1;
        static Random random = new Random();

        public DynaCommand GetNextCommand()
        {
            DynaCommand command = DynaCommand.CmdNothing;
            CallsUntilNextBomb--;

            #region -1. Get back to the map IF SAFE
            command = GetBackToMap();
            if (command != DynaCommand.CmdNothing)
            {
                CurrentBehaviorCommand = AIBehavior.GetBackToMap;
                if (ArgexAI.Map.DangerousToGoThere(command))
                    return DynaCommand.CmdNothing;
                else
                    return command;
            }
            #endregion
            #region 0. Self Defense
            command = SelfDefense();
            if (command != DynaCommand.CmdNothing)
            {
                CurrentBehaviorCommand = AIBehavior.SelfDefense;
                return command;
            }
            #endregion
            #region 1. Collect PowerUps
            command = CollectPowerups();
            if (command != DynaCommand.CmdNothing)
            {
                CurrentBehaviorCommand = AIBehavior.CollectPowerUp;
                return command;
            }
            #endregion
            #region 2. Move Towards Nearest Enemy
            command = MoveTowardsNearestEnemy();
            if (command != DynaCommand.CmdNothing)
            {
                CurrentBehaviorCommand = AIBehavior.MoveTowardsNearestEnemy;
                return command;
            }
            #endregion
            #region 3. Explode wall
            command = ExplodeNearestWall();
            if (command != DynaCommand.CmdNothing)
            {
                CurrentBehaviorCommand = AIBehavior.ExplodeNearestWall;
                return command;
            }
            #endregion
            #region 4. Random movement
            return RandomSafeMovement();
            #endregion

            //return DynaCommand.CmdNothing; //Needed if Random logic is commented
        }

        DynaCommand GetBackToMap()
        {
            //Are we standing at the border???
            Player myPlayer = ArgexAI.Map.MyPlayer;
            if (myPlayer.State == PlayerState.Standing)
            {
                if (myPlayer.TilePosX == 0)
                {
                    return DynaCommand.CmdRight;
                }
                if (myPlayer.TilePosY == 0)
                {
                    return DynaCommand.CmdDown;
                }
                if (myPlayer.TilePosX == Settings.General.MapSizeX - 1)
                {
                    return DynaCommand.CmdLeft;
                }
                if (myPlayer.TilePosY == Settings.General.MapSizeY - 1)
                {
                    return DynaCommand.CmdUp;
                }
            }
            return DynaCommand.CmdNothing;
        }
        DynaCommand SelfDefense()
        {
            //Should return a safe and possible command

            int myX = ArgexAI.Map.MyPlayer.TilePosX;
            int myY = ArgexAI.Map.MyPlayer.TilePosY;

            Map.DangerDirection dangerFrom = ArgexAI.Map.BombHarming(myX, myY);

            if (dangerFrom == Map.DangerDirection.NoDanger ||
                (ArgexAI.Map[myX, myY].Items.HasUndestroyableWall() && dangerFrom != Map.DangerDirection.InPlace) || //I'm in the wall (Got there with NoClip)
                ArgexAI.Map.MyPlayer.ArmorStepsRemaining > 1)
                return DynaCommand.CmdNothing;

            DynaCommand shouldDo = DynaCommand.CmdNothing;
            switch (dangerFrom)
            {
                #region InPlace
                case Map.DangerDirection.InPlace:
                    {
                        List<DynaCommand> WhereCanIGo = new List<DynaCommand>();
                        #region DangerCheck
                        //No Danger from LEFT?
                        if (myX > 0 &&
                            ArgexAI.Map[myX - 1, myY].CanWalkThrough &&
                            !ArgexAI.Map[myX - 1, myY].Items.HasFlame())
                        {
                            WhereCanIGo.Add(DynaCommand.CmdLeft);
                        }

                        //No Danger from RIGHT?
                        if (myX + 1 < Settings.General.MapSizeX &&
                            ArgexAI.Map[myX + 1, myY].CanWalkThrough &&
                            !ArgexAI.Map[myX + 1, myY].Items.HasFlame())
                        {
                            WhereCanIGo.Add(DynaCommand.CmdRight);
                        }

                        //No Danger from UP?
                        if (myY > 0 &&
                            ArgexAI.Map[myX, myY - 1].CanWalkThrough &&
                            !ArgexAI.Map[myX, myY - 1].Items.HasFlame())
                        {
                            WhereCanIGo.Add(DynaCommand.CmdUp);
                        }

                        //No Danger from DOWN?
                        if (myY + 1 < Settings.General.MapSizeY &&
                            ArgexAI.Map[myX, myY + 1].CanWalkThrough &&
                            !ArgexAI.Map[myX, myY + 1].Items.HasFlame())
                        {
                            WhereCanIGo.Add(DynaCommand.CmdDown);
                        }
                        #endregion

                        for (int i = WhereCanIGo.Count - 1; i >= 0; i--)
                        {
                            if (i == 0) //Last chance, we should to go there no matter what
                            {
                                shouldDo = WhereCanIGo[i];
                                break;
                            }

                            if (ArgexAI.Map.DeadEndThere(WhereCanIGo[i], myX, myY))
                            {
                                WhereCanIGo.RemoveAt(i);
                            }
                            else
                            {
                                shouldDo = WhereCanIGo[i];
                                break;
                            }
                        }


                        //shouldDo = (DynaCommand)random.Next(NumberOfDynaCommands);
                        break;
                    }
                #endregion
                #region Downwards
                case Map.DangerDirection.Downwards:
                    {
                        //Danger from LEFT
                        if (myX > 0 &&
                            ArgexAI.Map[myX - 1, myY].CanWalkThrough &&
                            !ArgexAI.Map[myX - 1, myY].Items.HasFlame() &&
                            ArgexAI.Map.BombHarming(myX - 1, myY) == Map.DangerDirection.NoDanger)
                        {
                            shouldDo = DynaCommand.CmdLeft;
                            break;
                        }

                        //Danger from RIGHT
                        if (myX + 1 < Settings.General.MapSizeX &&
                            ArgexAI.Map[myX + 1, myY].CanWalkThrough &&
                            !ArgexAI.Map[myX + 1, myY].Items.HasFlame() &&
                            ArgexAI.Map.BombHarming(myX + 1, myY) == Map.DangerDirection.NoDanger)
                        {
                            shouldDo = DynaCommand.CmdRight;
                            break;
                        }

                        shouldDo = DynaCommand.CmdUp;

                        break;
                    }
                #endregion
                #region Upwards
                case Map.DangerDirection.Upwards:
                    {
                        //Danger from LEFT
                        if (myX > 0 &&
                            ArgexAI.Map[myX - 1, myY].CanWalkThrough &&
                            !ArgexAI.Map[myX - 1, myY].Items.HasFlame() &&
                            ArgexAI.Map.BombHarming(myX - 1, myY) == Map.DangerDirection.NoDanger)
                        {
                            shouldDo = DynaCommand.CmdLeft;
                            break;
                        }

                        //Danger from RIGHT
                        if (myX + 1 < Settings.General.MapSizeX &&
                            ArgexAI.Map[myX + 1, myY].CanWalkThrough &&
                            !ArgexAI.Map[myX + 1, myY].Items.HasFlame() &&
                            ArgexAI.Map.BombHarming(myX + 1, myY) == Map.DangerDirection.NoDanger)
                        {
                            shouldDo = DynaCommand.CmdRight;
                            break;
                        }

                        shouldDo = DynaCommand.CmdDown;

                        break;
                    }
                #endregion
                #region Left
                case Map.DangerDirection.Left:
                    {
                        //Danger from UP
                        if (myY > 0 &&
                            ArgexAI.Map[myX, myY - 1].CanWalkThrough &&
                            !ArgexAI.Map[myX, myY - 1].Items.HasFlame() &&
                            ArgexAI.Map.BombHarming(myX, myY - 1) == Map.DangerDirection.NoDanger)
                        {
                            shouldDo = DynaCommand.CmdUp;
                            break;
                        }

                        //Danger from DOWN
                        if (myX + 1 < Settings.General.MapSizeY &&
                            ArgexAI.Map[myX, myY + 1].CanWalkThrough &&
                            !ArgexAI.Map[myX, myY + 1].Items.HasFlame() &&
                            ArgexAI.Map.BombHarming(myX, myY + 1) == Map.DangerDirection.NoDanger)
                        {
                            shouldDo = DynaCommand.CmdDown;
                            break;
                        }

                        shouldDo = DynaCommand.CmdRight;

                        break;
                    }
                #endregion
                #region Right
                case Map.DangerDirection.Right:
                    {
                        //Danger from UP
                        if (myY > 0 &&
                            ArgexAI.Map[myX, myY - 1].CanWalkThrough &&
                            !ArgexAI.Map[myX, myY - 1].Items.HasFlame() &&
                            ArgexAI.Map.BombHarming(myX, myY - 1) == Map.DangerDirection.NoDanger)
                        {
                            shouldDo = DynaCommand.CmdUp;
                            break;
                        }

                        //Danger from DOWN
                        if (myY + 1 < Settings.General.MapSizeY &&
                            ArgexAI.Map[myX, myY + 1].CanWalkThrough &&
                            !ArgexAI.Map[myX, myY + 1].Items.HasFlame() &&
                            ArgexAI.Map.BombHarming(myX, myY + 1) == Map.DangerDirection.NoDanger)
                        {
                            shouldDo = DynaCommand.CmdDown;
                            break;
                        }

                        shouldDo = DynaCommand.CmdLeft;

                        break;
                    }
                    #endregion
            }

            #region Reverse handling
            if ((shouldDo == DynaCommand.CmdLeft && ArgexAI.Map.MyPlayer.DX == 1) ||
                (shouldDo == DynaCommand.CmdRight && ArgexAI.Map.MyPlayer.DX == -1) ||
                (shouldDo == DynaCommand.CmdUp && ArgexAI.Map.MyPlayer.DY == 1) ||
                (shouldDo == DynaCommand.CmdDown && ArgexAI.Map.MyPlayer.DY == -1))
            {
                shouldDo = DynaCommand.CmdReverse;
            }
            #endregion

            //Can I go there?
            shouldDo = ArgexAI.Map.RemoveInvalidStep(shouldDo);

            //Is it dangerous to go there?
            if (ArgexAI.Map.DangerousToGoThere(shouldDo) || shouldDo == DynaCommand.CmdNothing)
                return DynaCommand.CmdNothing;

            return shouldDo; //PLACEHOLDER
        }
        DynaCommand CollectPowerups()
        {
            //Should return a safe and possible command
            int myX = ArgexAI.Map.MyPlayer.TilePosX;
            int myY = ArgexAI.Map.MyPlayer.TilePosY;

            #region If we can bomb and have ENEMY in bomb range, place a bomb (if we can by the CallsUntilNextBomb)
            if (CallsUntilNextBomb <= 0 && ArgexAI.Map.ActiveEnemyInBombRangeThere(myX, myY) && !DangerousToBombThere())
                return DynaCommand.CmdBomb;
            #endregion

            if (ArgexAI.Map.PowerUps.Count == 0)
                return DynaCommand.CmdNothing;

            PowerUp nearest;
            int[,] waveMapToNearest = new int[0, 0];
            int minDistance = int.MaxValue;

            #region Find nearest
            var allWithoutALWAYSBOMB = ArgexAI.Map.PowerUps.Where(s => s.TileChar != 'U');
            foreach (var item in allWithoutALWAYSBOMB)
            {
                ////////Stopwatch sw = Stopwatch.StartNew();        //Performance test 1/2
                GameLogic.WaveFrontExpansion.WaveFrontExpAlg h = new GameLogic.WaveFrontExpansion.WaveFrontExpAlg(ArgexAI.Map, item);
                item.DistanceFromUs = h.Distance();
                ////////Console.WriteLine(sw.Elapsed.ToString());   //Performance test 2/2
                //Logger.LogIntMap(h.CurrentWaveMap);

                if (item.DistanceFromUs < minDistance)
                {
                    nearest = item;
                    minDistance = item.DistanceFromUs;
                    waveMapToNearest = h.CurrentWaveMap;
                }
            }
            #endregion

            if (minDistance == int.MaxValue)
                return DynaCommand.CmdNothing;

            #region Next Step
            int minVal = int.MaxValue;
            DynaCommand command = DynaCommand.CmdNothing;

            if (myY > 0)
            {
                int UP = waveMapToNearest[myX, myY - 1];
                if (UP == -3)
                {
                    return DynaCommand.CmdUp;
                }
                if (UP < minVal && UP > 0)
                {
                    command = DynaCommand.CmdUp;
                    minVal = UP;
                }
            }
            if (myY + 1 < Settings.General.MapSizeY)
            {
                int DOWN = waveMapToNearest[myX, myY + 1];
                if (DOWN == -3)
                {
                    return DynaCommand.CmdDown;
                }
                if (DOWN < minVal && DOWN > 0)
                {
                    command = DynaCommand.CmdDown;
                    minVal = DOWN;
                }
            }
            if (myX + 1 < Settings.General.MapSizeX)
            {
                int RIGHT = waveMapToNearest[myX + 1, myY];
                if (RIGHT == -3)
                {
                    return DynaCommand.CmdRight;
                }
                if (RIGHT < minVal && RIGHT > 0)
                {
                    command = DynaCommand.CmdRight;
                    minVal = RIGHT;
                }
            }
            if (myX > 0)
            {
                int LEFT = waveMapToNearest[myX - 1, myY];
                if (LEFT == -3)
                {
                    return DynaCommand.CmdLeft;
                }
                if (LEFT < minVal && LEFT > 0)
                {
                    command = DynaCommand.CmdLeft;
                    minVal = LEFT;
                }
            }

            #endregion

            //Can I go there?
            command = ArgexAI.Map.RemoveInvalidStep(command);

            if (command == DynaCommand.CmdNothing)
            {
                return DynaCommand.CmdNothing;
            }

            //Is it dangerous to go there?
            if (ArgexAI.Map.DangerousToGoThere(command)) // || command == DynaCommand.CmdNothing)
                return DynaCommand.CmdStop;

            return command;
        }
        DynaCommand MoveTowardsNearestEnemy()
        {
            //Should return a safe and possible command
            int myX = ArgexAI.Map.MyPlayer.TilePosX;
            int myY = ArgexAI.Map.MyPlayer.TilePosY;
            DynaCommand command = DynaCommand.CmdNothing;

            //If someone is in bomb placing range, I don't have to check it because CollectPowerups() method checked it already (and placed a bomb)

            #region Find nearest enemy

            Player nearest;
            int[,] waveMapToNearest = new int[0, 0];
            int minDistance = int.MaxValue;
            List<Player> enemies = ArgexAI.Map.Enemies.Where(   //Active and vulnerable enemies
                s => s.IsActive == true &&
                s.NickName != Settings.FromServer.PLAYER.DEFAULTNAME &&
                s.ArmorStepsRemaining <= 0).ToList();

            foreach (var actEnemy in enemies)
            {
                GameLogic.WaveFrontExpansion.WaveFrontExpAlg h = new GameLogic.WaveFrontExpansion.WaveFrontExpAlg(ArgexAI.Map, actEnemy);
                actEnemy.DistanceFromUs = h.Distance();

                if (actEnemy.DistanceFromUs < minDistance)
                {
                    nearest = actEnemy;
                    minDistance = actEnemy.DistanceFromUs;
                    waveMapToNearest = h.CurrentWaveMap;
                }
            }

            #endregion

            if (minDistance == int.MaxValue) return DynaCommand.CmdNothing; //No enemy in the zone

            #region Move towards the nearest enemy

            int minValue = int.MaxValue;
            if (myY > 0)
            {
                int UP = waveMapToNearest[myX, myY - 1];
                if (UP == -3) return DynaCommand.CmdNothing;
                if (UP < minValue && UP > 0)
                {
                    command = DynaCommand.CmdUp;
                    minValue = UP;
                }
            }
            if (myY + 1 < Settings.General.MapSizeY)
            {
                int DOWN = waveMapToNearest[myX, myY + 1];
                if (DOWN == -3) return DynaCommand.CmdNothing;
                if (DOWN < minValue && DOWN > 0)
                {
                    command = DynaCommand.CmdDown;
                    minValue = DOWN;
                }
            }

            if (myX + 1 < Settings.General.MapSizeX)
            {
                int RIGHT = waveMapToNearest[myX + 1, myY];
                if (RIGHT == -3) return DynaCommand.CmdNothing;
                if (RIGHT < minValue && RIGHT > 0)
                {
                    command = DynaCommand.CmdRight;
                    minValue = RIGHT;
                }
            }

            if (myX > 0)
            {
                int LEFT = waveMapToNearest[myX - 1, myY];
                if (LEFT == -3) return DynaCommand.CmdNothing;
                if (LEFT < minValue && LEFT > 0)
                {
                    command = DynaCommand.CmdLeft;
                    minValue = LEFT;
                }
            }

            #endregion

            //Is it not possible or dangerous to go there?
            if (!ArgexAI.Map.MyPlayerCanGoThere(command) || ArgexAI.Map.DangerousToGoThere(command) || command == DynaCommand.CmdNothing)
                return DynaCommand.CmdNothing;

            return command;
        }
        DynaCommand ExplodeNearestWall()
        {
            //Should return a safe and possible command

            int myX = ArgexAI.Map.MyPlayer.TilePosX;
            int myY = ArgexAI.Map.MyPlayer.TilePosY;

            #region If we can bomb and have WALL next to us, place a bomb (if we can by the CallsUntilNextBomb)
            if (CallsUntilNextBomb <= 0 &&
                ((myX + 1 < Settings.General.MapSizeX && ArgexAI.Map[myX + 1, myY].Items.HasExplodableWall()) ||
                (myX > 0 && ArgexAI.Map[myX - 1, myY].Items.HasExplodableWall()) ||
                (myY + 1 < Settings.General.MapSizeY && ArgexAI.Map[myX, myY + 1].Items.HasExplodableWall()) ||
                (myY > 0 && ArgexAI.Map[myX, myY - 1].Items.HasExplodableWall()))
                 && !DangerousToBombThere())
            {
                return DynaCommand.CmdBomb;
            }
            #endregion

            #region Find a wall
            Queue<Point> que = new Queue<Point>();
            List<Point> checkedPoints = new List<Point>();
            que.Enqueue(new Point(myX, myY));

            bool found = false;
            GameItemCollection targetToGo = new GameItemCollection();

            while (que.Count > 0 && !found)
            {
                Point currPoint = que.Dequeue();
                checkedPoints.Add(currPoint);
                if (!MapActualizer.IsInsideTheMap(currPoint.X, currPoint.Y))
                    continue;

                GameItemCollection currentItems = ArgexAI.Map[currPoint.X, currPoint.Y];

                if (currentItems.Items.HasUndestroyableWall() &&    //If the item is an undestroyable wall and I am not standing in it
                    !(currentItems.Items[0].PosX == myX && currentItems.Items[0].PosY == myY)
                    )
                {
                    continue;
                }

                if (currentItems.Items.HasExplodableWall())
                {
                    found = true;
                    targetToGo = currentItems;
                    break;
                }
                else
                {
                    //LEFT
                    if (currPoint.X > 0 &&
                        !checkedPoints.Any(p => p.X == currPoint.X - 1 && p.Y == currPoint.Y) &&
                        !que.Any(p => p.X == currPoint.X - 1 && p.Y == currPoint.Y)
                        )
                    {
                        que.Enqueue(new Point(currPoint.X - 1, currPoint.Y));
                    }
                    //RIGHT
                    if (currPoint.X + 1 < Settings.General.MapSizeX &&
                        !checkedPoints.Any(p => p.X == currPoint.X + 1 && p.Y == currPoint.Y) &&
                        !que.Any(p => p.X == currPoint.X + 1 && p.Y == currPoint.Y)
                        )
                    {
                        que.Enqueue(new Point(currPoint.X + 1, currPoint.Y));
                    }
                    //UP
                    if (currPoint.Y > 0 &&
                        !checkedPoints.Any(p => p.X == currPoint.X && p.Y == currPoint.Y - 1) &&
                        !que.Any(p => p.X == currPoint.X && p.Y == currPoint.Y - 1)
                        )
                    {
                        que.Enqueue(new Point(currPoint.X, currPoint.Y - 1));
                    }
                    //DOWN
                    if (currPoint.Y + 1 < Settings.General.MapSizeY &&
                        !checkedPoints.Any(p => p.X == currPoint.X && p.Y == currPoint.Y + 1) &&
                        !que.Any(p => p.X == currPoint.X && p.Y == currPoint.Y + 1)
                        )
                    {
                        que.Enqueue(new Point(currPoint.X, currPoint.Y + 1));
                    }
                }
            }
            #endregion

            if (!found)
                return DynaCommand.CmdNothing;

            #region Next Step
            //I have to go towards "targetToGo". Where should I head to?
            GameLogic.WaveFrontExpansion.WaveFrontExpAlg h = new GameLogic.WaveFrontExpansion.WaveFrontExpAlg(ArgexAI.Map, targetToGo.Items[0]);
            h.Distance();
            int[,] waveMapToNearest = h.CurrentWaveMap;

            int minVal = int.MaxValue;
            DynaCommand command = DynaCommand.CmdNothing;

            if (myY > 0)
            {
                int UP = waveMapToNearest[myX, myY - 1];
                if (UP == -3) return DynaCommand.CmdNothing;
                if (UP < minVal && UP > 0)
                {
                    command = DynaCommand.CmdUp;
                    minVal = UP;
                }
            }
            if (myY + 1 < Settings.General.MapSizeY)
            {
                int DOWN = waveMapToNearest[myX, myY + 1];
                if (DOWN == -3) return DynaCommand.CmdNothing;
                if (DOWN < minVal && DOWN > 0)
                {
                    command = DynaCommand.CmdDown;
                    minVal = DOWN;
                }
            }

            if (myX + 1 < Settings.General.MapSizeX)
            {
                int RIGHT = waveMapToNearest[myX + 1, myY];
                if (RIGHT == -3) return DynaCommand.CmdNothing;
                if (RIGHT < minVal && RIGHT > 0)
                {
                    command = DynaCommand.CmdRight;
                    minVal = RIGHT;
                }
            }

            if (myX > 0)
            {
                int LEFT = waveMapToNearest[myX - 1, myY];
                if (LEFT == -3) return DynaCommand.CmdNothing;
                if (LEFT < minVal && LEFT > 0)
                {
                    command = DynaCommand.CmdLeft;
                    minVal = LEFT;
                }
            }

            #endregion

            //Is it not possible or dangerous to go there?
            if (!ArgexAI.Map.MyPlayerCanGoThere(command) || ArgexAI.Map.DangerousToGoThere(command) || command == DynaCommand.CmdNothing)
                return DynaCommand.CmdNothing;

            return command;
        }
        public DynaCommand RandomSafeMovement()
        {
            DynaCommand command = (DynaCommand)random.Next(5); //CmdDown, CmdUp, CmdLeft, CmdRight, CmdStop
            CurrentBehaviorCommand = AIBehavior.Random;
            if (ArgexAI.Map.DangerousToGoThere(command) || !ArgexAI.Map.MyPlayerCanGoThere(command))
                return DynaCommand.CmdNothing;
            else
                return command;
        }

        bool DangerousToBombThere()
        {
            int myX = ArgexAI.Map.MyPlayer.TilePosX;
            int myY = ArgexAI.Map.MyPlayer.TilePosY;
            Bomb virtualBomb = new Bomb();

            ArgexAI.Map[myX, myY].Items.Add(virtualBomb); //Let's "virtually" place the bomb

            //If I place the bomb, can I get to a safe spot?

            List<GameItemCollection> safePlacesToGo = new List<GameItemCollection>();
            #region Find all nearby SAFE places where I can walk from here
            int counterForMaxElements = 25;
            Queue<Point> que = new Queue<Point>();
            List<Point> checkedPoints = new List<Point>();

            #region Things to do at the FIRST element (me)

            Point myPoint = new Point(myX, myY);
            checkedPoints.Add(myPoint);
            //LEFT
            if (myPoint.X > 0 &&
                ArgexAI.Map[myPoint.X - 1, myPoint.Y].CanWalkThrough &&
                !ArgexAI.Map[myPoint.X - 1, myPoint.Y].Items.HasFlame())
                que.Enqueue(new Point(myPoint.X - 1, myPoint.Y));
            //RIGHT
            if (myPoint.X + 1< Settings.General.MapSizeX &&
                ArgexAI.Map[myPoint.X + 1, myPoint.Y].CanWalkThrough &&
                !ArgexAI.Map[myPoint.X + 1, myPoint.Y].Items.HasFlame())
                que.Enqueue(new Point(myPoint.X + 1, myPoint.Y));
            //UP
            if (myPoint.Y > 0 &&
                ArgexAI.Map[myPoint.X, myPoint.Y - 1].CanWalkThrough &&
                !ArgexAI.Map[myPoint.X, myPoint.Y - 1].Items.HasFlame())
                que.Enqueue(new Point(myPoint.X, myPoint.Y - 1));
            //DOWN
            if (myPoint.Y + 1 < Settings.General.MapSizeY &&
                ArgexAI.Map[myPoint.X, myPoint.Y + 1].CanWalkThrough &&
                !ArgexAI.Map[myPoint.X, myPoint.Y + 1].Items.HasFlame())
                que.Enqueue(new Point(myPoint.X, myPoint.Y + 1));

            #endregion
            #region Other elements
            while (que.Count > 0)
            {
                Point currPoint = que.Dequeue();
                checkedPoints.Add(currPoint);
                if (!MapActualizer.IsInsideTheMap(currPoint.X, currPoint.Y))
                    continue;

                GameItemCollection currentTILE = ArgexAI.Map[currPoint.X, currPoint.Y];

                if (--counterForMaxElements <= 0)
                    break;

                if (ArgexAI.Map.BombHarming(currPoint.X, currPoint.Y) == Map.DangerDirection.NoDanger &&
                    !currentTILE.Items.HasFlame())
                {
                    safePlacesToGo.Add(currentTILE);
                }
                else
                {
                    //LEFT
                    if (currPoint.X > 0 &&
                        !checkedPoints.Any(p => p.X == currPoint.X - 1 && p.Y == currPoint.Y) &&
                        !que.Any(p => p.X == currPoint.X - 1 && p.Y == currPoint.Y) &&
                        ArgexAI.Map[currPoint.X - 1, currPoint.Y].CanWalkThrough &&
                        !ArgexAI.Map[currPoint.X - 1, currPoint.Y].Items.HasFlame())
                    {
                        que.Enqueue(new Point(currPoint.X - 1, currPoint.Y));
                    }
                    //RIGHT
                    if (currPoint.X + 1 < Settings.General.MapSizeX &&
                        !checkedPoints.Any(p => p.X == currPoint.X + 1 && p.Y == currPoint.Y) &&
                        !que.Any(p => p.X == currPoint.X + 1 && p.Y == currPoint.Y) &&
                        ArgexAI.Map[currPoint.X + 1, currPoint.Y].CanWalkThrough &&
                        !ArgexAI.Map[currPoint.X + 1, currPoint.Y].Items.HasFlame())
                    {
                        que.Enqueue(new Point(currPoint.X + 1, currPoint.Y));
                    }
                    //UP
                    if (currPoint.Y > 0 &&
                        !checkedPoints.Any(p => p.X == currPoint.X && p.Y == currPoint.Y - 1) &&
                        !que.Any(p => p.X == currPoint.X && p.Y == currPoint.Y - 1) &&
                        ArgexAI.Map[currPoint.X, currPoint.Y - 1].CanWalkThrough &&
                        !ArgexAI.Map[currPoint.X, currPoint.Y - 1].Items.HasFlame())
                    {
                        que.Enqueue(new Point(currPoint.X, currPoint.Y - 1));
                    }
                    //DOWN
                    if (currPoint.Y + 1 < Settings.General.MapSizeY &&
                        !checkedPoints.Any(p => p.X == currPoint.X && p.Y == currPoint.Y + 1) &&
                        !que.Any(p => p.X == currPoint.X && p.Y == currPoint.Y + 1) &&
                        ArgexAI.Map[currPoint.X, currPoint.Y + 1].CanWalkThrough &&
                        !ArgexAI.Map[currPoint.X, currPoint.Y + 1].Items.HasFlame())
                    {
                        que.Enqueue(new Point(currPoint.X, currPoint.Y + 1));
                    }
                }
            }
            #endregion
            #endregion

            #region Find the distance to the nearest SAFE spot
            int minDistance = int.MaxValue;
            foreach (GameItemCollection item in safePlacesToGo)
            {
                GameLogic.WaveFrontExpansion.WaveFrontExpAlg h = new GameLogic.WaveFrontExpansion.WaveFrontExpAlg(ArgexAI.Map, item.Items[0]);
                int dist = h.Distance();
                if (dist < minDistance)
                    minDistance = dist;
            }
            #endregion

            ArgexAI.Map[myX, myY].Items.Remove(virtualBomb);

            //If the distance <= 3, it's not dangerous to place
            if (minDistance <= 3)
                return false;
            else
                return true;
        }
        public static bool IsMovingCommand(DynaCommand command)
        {
            return command == DynaCommand.CmdRight ||
                command == DynaCommand.CmdLeft ||
                command == DynaCommand.CmdUp ||
                command == DynaCommand.CmdDown;
        }
    }
    public enum AIBehavior
    {
        INITIAL, GetBackToMap, SelfDefense, CollectPowerUp, MoveTowardsNearestEnemy, ExplodeNearestWall, Random,
        DirectionChange, StopBeforeBomb, NoBombStepsHANDLING, OnlyStopWhenBetweenTile
    }
}