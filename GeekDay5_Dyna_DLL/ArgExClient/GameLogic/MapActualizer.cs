﻿using DynaCommon;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ArgExClient
{
    static class MapActualizer
    {
        public static void Actualize(long elapsedTime)
        {
            try
            {

                for (int i = 0; i < elapsedTime; i++)
                {
                    ActualizeOnce();
                }
                #region Set itemsReady to true for all elements
                for (int i = 0; i < Settings.General.MapSizeX; i++)
                    for (int j = 0; j < Settings.General.MapSizeY; j++)
                        ArgexAI.Map[i, j].ItemsReady = true;
                #endregion
            }
            catch(Exception e)
            {
                Logger.Log("Actualize ERROR: " + e.Message);
            }
            //            Logger.LogMap(ArgexAI.Map.Items);
            //#warning Logger.LogMap(Map.Items); STAYED in code
        }
        private static void ActualizeOnce()
        {
            //ArgexAI.Map
            //ArgexAI.CurrentServerRenderTicks
            //MyPlayer, Bombs & flames timing

            Player myPlayer = ArgexAI.Map.MyPlayer;
            int myLastTilePosX = myPlayer.TilePosX;
            int myLastTilePosY = myPlayer.TilePosY;

            #region MyPlayer
            if (myPlayer.State != PlayerState.Standing)
            {
                if (myPlayer.State == PlayerState.InTile)
                {
                    int px = (int)Math.Round(myPlayer.PosX);
                    int py = (int)Math.Round(myPlayer.PosY);
                    #region PowerUp updates
                    if (myPlayer.ArmorStepsRemaining > 0) myPlayer.ArmorStepsRemaining--;
                    if (myPlayer.SpeedUpStepsRemaining > 0) myPlayer.SpeedUpStepsRemaining--;
                    if (myPlayer.NoBombStepsRemaining > 0) myPlayer.NoBombStepsRemaining--;
                    if (myPlayer.SpeedDownStepsRemaining > 0) myPlayer.SpeedDownStepsRemaining--;
                    if (myPlayer.AlwaysBombStepsRemaining > 0) myPlayer.AlwaysBombStepsRemaining--;
                    if (myPlayer.AlwaysBombStepsRemaining > 0)
                    {
                        Bomb bomb = new Bomb(px, py);
                        ArgexAI.Map[px, py].Items.Add(bomb);
                        ArgexAI.Map.Bombs.Add(bomb);
                    }
                    #endregion
                    #region Collect secrets
                    List<PowerUp> secrets = ArgexAI.Map.PowerUps.Where(s => s.PosX == px && s.PosY == py).ToList();
                    if (secrets.Count > 0)
                    {
                        for (int i = secrets.Count - 1; i >= 0; i--)
                        {
                            switch (secrets[i].TileChar)
                            {
                                case 'O': break;
                                case 'P': myPlayer.NoClipStepsRemaining = Settings.FromServer.PLAYER.NOCLIPSTEPS; break;
                                case 'Q': myPlayer.ArmorStepsRemaining = Settings.FromServer.PLAYER.SECRETSTEPS; break;
                                case 'R': myPlayer.SpeedUpStepsRemaining = Settings.FromServer.PLAYER.SECRETSTEPS; break;
                                case 'S': myPlayer.NoBombStepsRemaining = Settings.FromServer.PLAYER.SECRETSTEPS; break;
                                case 'T': myPlayer.SpeedDownStepsRemaining = Settings.FromServer.PLAYER.SECRETSTEPS; break;
                                case 'U': myPlayer.AlwaysBombStepsRemaining = Settings.FromServer.PLAYER.SECRETSTEPS; break;
                            }

                            ArgexAI.Map.PowerUps.Remove(secrets[i]);
                            ArgexAI.Map[px, py].Items.Remove(secrets[i]);
                        }
                    }
                    #endregion
                    #region Stop or move to next tile
                    int NextX = px + myPlayer.DX;
                    int NextY = py + myPlayer.DY;

                    if (myPlayer.MustStop ||
                       (ArgexAI.Map[NextX, NextY].Items.HasUndestroyableWall() && myPlayer.NoClipStepsRemaining <= 0) ||
                       (ArgexAI.Map[NextX, NextY].Items.HasBomb() && myPlayer.NoClipStepsRemaining <= 0))
                    {
                        myPlayer.DX = 0; myPlayer.DY = 0;
                        myPlayer.State = PlayerState.Standing;
                    }
                    else
                    {
                        if (!ArgexAI.Map[NextX, NextY].CanWalkThrough)
                        {
                            myPlayer.NoClipStepsRemaining--;
                        }
                        myPlayer.State = PlayerState.BetweenTiles;
                        myPlayer.PosX += myPlayer.CurrentWalkSpeed * myPlayer.DX;
                        myPlayer.PosY += myPlayer.CurrentWalkSpeed * myPlayer.DY;
                    }
                    #endregion
                }
                else
                {
                    double dx = myPlayer.CurrentWalkSpeed * myPlayer.DX;
                    double dy = myPlayer.CurrentWalkSpeed * myPlayer.DY;
                    myPlayer.PosX += dx;
                    myPlayer.PosY += dy;
                    if (IsInTile(myPlayer.PosX, dx) || IsInTile(myPlayer.PosY, dy))
                    {
                        myPlayer.PosX = Math.Round(myPlayer.PosX);
                        myPlayer.PosY = Math.Round(myPlayer.PosY);
                        myPlayer.State = PlayerState.InTile;
                    }
                }
            }
            myPlayer.TilePosX = (int)Math.Round(myPlayer.PosX);
            myPlayer.TilePosY = (int)Math.Round(myPlayer.PosY);
            ArgexAI.Map.Items[myLastTilePosX, myLastTilePosY].Items.Remove(myPlayer);
            ArgexAI.Map.Items[myPlayer.TilePosX, myPlayer.TilePosY].Items.Add(myPlayer);
            #endregion
            #region Tick bombs and flames
            List<Bomb> BombsToExplode = new List<Bomb>();
            foreach (Bomb akt in ArgexAI.Map.Bombs)
            {
                akt.TimeLeft--;
                if (akt.TimeLeft <= 0)
                {
                    BombsToExplode.Add(akt);
                }
            }

            List<Flame> FlamesToRemove = new List<Flame>();
            foreach (Flame akt in ArgexAI.Map.Flames)
            {
                akt.TimeLeft--;
                if (akt.TimeLeft <= 0)
                {
                    FlamesToRemove.Add(akt);
                }
            }

            #endregion
            #region Explode bombs, place flames, remove walls
            for (int i = 0; i < BombsToExplode.Count; i++)
            {
                Bomb akt = BombsToExplode[i];
                ArgexAI.Map.Bombs.Remove(akt);
                int posX = (int)akt.PosX;
                int posY = (int)akt.PosY;

                Flame f = new Flame(posX, posY, Settings.FromServer.LEVEL.BOMBTIME);
                ArgexAI.Map.Flames.Add(f);
                ArgexAI.Map.Items[posX, posY].Items.Add(f);

                for (int y = posY - 1; y >= posY - myPlayer.BombSize; y--)
                {
                    if (PutFlame(akt, posX, y, BombsToExplode, ArgexAI.Map.Bombs)) break;
                }
                for (int y = posY + 1; y <= posY + myPlayer.BombSize; y++)
                {
                    if (PutFlame(akt, posX, y, BombsToExplode, ArgexAI.Map.Bombs)) break;
                }
                for (int x = posX - 1; x >= posX - myPlayer.BombSize; x--)
                {
                    if (PutFlame(akt, x, posY, BombsToExplode, ArgexAI.Map.Bombs)) break;
                }
                for (int x = posX + 1; x <= posX + myPlayer.BombSize; x++)
                {
                    if (PutFlame(akt, x, posY, BombsToExplode, ArgexAI.Map.Bombs)) break;
                }
            }
            #endregion

            // Remove flames
            foreach (Flame akt in FlamesToRemove)
            { ArgexAI.Map.Flames.Remove(akt); }

        }
        private static bool IsInTile(double value, double diff)
        {
            double target = Math.Round(value);
            double v1 = value - diff;
            double v2 = value;
            double v3 = value + diff;
            double diff1 = Math.Abs(target - v1);
            double diff2 = Math.Abs(target - v2);
            double diff3 = Math.Abs(target - v3);
            return diff1 > diff2 && diff2 <= diff3;
        }
        private static bool PutFlame(Bomb akt, int posX, int posY, List<Bomb> BombsToExplode, List<Bomb> Bombs)
        {
            if (!IsInsideTheMap(posX, posY)) return true;

            BombsToExplode.AddRange(Bombs.Where(x => x.PosX == posX && x.PosY == posY));
            foreach (Bomb aktBomb in BombsToExplode) Bombs.Remove(aktBomb);

            Flame Flame = new Flame(posX, posY, Settings.FromServer.LEVEL.BOMBTIME);

            if (!ArgexAI.Map[posX, posY].Items.HasUndestroyableWall())
            {
                ArgexAI.Map.Flames.Add(Flame);
                ArgexAI.Map.Items[posX, posY].Items.Add(Flame);
                return false;
            }
            else if (ArgexAI.Map[posX, posY].Items.HasBombOrExplodableWall())
            {
                ArgexAI.Map.Items[posX, posY].Items.RemoveAll(s => s.TileChar == 'V');
                ArgexAI.Map.Items[posX, posY].Items.RemoveAll(s => s.TileChar == 'Y');

                ArgexAI.Map.Flames.Add(Flame);
                ArgexAI.Map.Items[posX, posY].Items.Add(Flame);

                if (ArgexAI.Map[posX, posY].Items.HasExplodableWall()) //ADDED BY ARPHOX
                    return true; //If we manage to boom a wall, I don't want to place more flames to that direction
            }
            return true;
        }
        public static bool IsInsideTheMap(int x, int y)
        {
            return x >= 0 && x < Settings.General.MapSizeX && y >= 0 && y < Settings.General.MapSizeY;
        }

        public static void ActualizeAfterCommand(DynaCommand command)
        {
            try {
                int myX = ArgexAI.Map.MyPlayer.TilePosX;
                int myY = ArgexAI.Map.MyPlayer.TilePosY;
                Player myPlayer = ArgexAI.Map.MyPlayer;

                #region If I would move into a wall, throw exception
                switch (command)
                {
                    case DynaCommand.CmdLeft:
                        if (!ArgexAI.Map.Items[myX - 1, myY].CanWalkThrough && ArgexAI.Map.Items[myX, myY].CanWalkThrough)
                            throw new ApplicationException("I want to move left when I can't! (ActualizeAfterCommand)");
                        break;
                    case DynaCommand.CmdRight:
                        if (!ArgexAI.Map.Items[myX + 1, myY].CanWalkThrough && ArgexAI.Map.Items[myX, myY].CanWalkThrough)
                            throw new ApplicationException("I want to move right when I can't! (ActualizeAfterCommand)");
                        break;
                    case DynaCommand.CmdUp:
                        if (!ArgexAI.Map.Items[myX, myY - 1].CanWalkThrough && ArgexAI.Map.Items[myX, myY].CanWalkThrough)
                            throw new ApplicationException("I want to move up when I can't! (ActualizeAfterCommand)");
                        break;
                    case DynaCommand.CmdDown:
                        if (!ArgexAI.Map.Items[myX, myY + 1].CanWalkThrough && ArgexAI.Map.Items[myX, myY].CanWalkThrough)
                            throw new ApplicationException("I want to move down when I can't! (ActualizeAfterCommand)");
                        break;
                }
                #endregion

                switch (command)
                {
                    #region Bomb
                    case DynaCommand.CmdBomb:
                        {
                            Bomb bomb = new Bomb(myX, myY);
                            ArgexAI.Map[myX, myY].Items.Add(bomb);
                            ArgexAI.Map.Bombs.Add(bomb);
                            ArgexAI.Map[myX, myY].ItemsReady = true;    //So he Calculates WalkThrough again.
                            break;
                        }
                    #endregion
                    #region Stop
                    case DynaCommand.CmdStop:
                        {
                            if (myPlayer.State != PlayerState.Standing)
                            {
                                myPlayer.MustStop = true;
                                myPlayer.DX = 0;
                                myPlayer.DY = 0;
                            }
                            break;
                        }
                    #endregion
                    #region Reverse
                    case DynaCommand.CmdReverse:
                        {
                            myPlayer.DX = -myPlayer.DX;
                            myPlayer.DY = -myPlayer.DY;
                            break;
                        }
                    #endregion
                    #region Down
                    case DynaCommand.CmdDown:
                        {
                            if (myPlayer.State == PlayerState.Standing)
                            {
                                myPlayer.DX = 0;
                                myPlayer.DY = 1;
                                myPlayer.State = PlayerState.InTile;
                                myPlayer.MustStop = false;
                            }
                            break;
                        }
                    #endregion
                    #region Up
                    case DynaCommand.CmdUp:
                        {
                            if (myPlayer.State == PlayerState.Standing)
                            {
                                myPlayer.DX = 0;
                                myPlayer.DY = -1;
                                myPlayer.State = PlayerState.InTile;
                                myPlayer.MustStop = false;
                            }
                            break;
                        }
                    #endregion
                    #region Left
                    case DynaCommand.CmdLeft:
                        {
                            if (myPlayer.State == PlayerState.Standing)
                            {
                                myPlayer.DX = -1;
                                myPlayer.DY = 0;
                                myPlayer.State = PlayerState.InTile;
                                myPlayer.MustStop = false;
                            }
                            break;
                        }
                    #endregion
                    #region Right
                    case DynaCommand.CmdRight:
                        {
                            if (myPlayer.State == PlayerState.Standing)
                            {
                                myPlayer.DX = 1;
                                myPlayer.DY = 0;
                                myPlayer.State = PlayerState.InTile;
                                myPlayer.MustStop = false;
                            }
                            break;
                        }
                        #endregion
                }
            }
            catch (Exception e)
            {
                Logger.Log("ActualizeAfterCommand ERROR: " + e.Message);
            }
            //            Logger.LogMap(ArgexAI.Map.Items);
            //#warning Logger.LogMap(Map.Items); STAYED in code
        }
    }
}