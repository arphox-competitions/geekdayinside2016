﻿using DynaCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
// Must add references to: PresentationCore, System.Xaml, WindowsBase
// Must use .NET 4.5.0

namespace DynaClientAnon
{
    public class AnonClient : IDynaClient
    {
        static Random R = new Random();
        int maxNum;
        string nickName;

        public AnonClient()
        {
            maxNum = Enum.GetValues(typeof(DynaCommand)).Length;
            nickName = "szabozs éáéáé \a xxx őőő " + R.Next(1000, 10000);
        }

        public string NickName
        {
            get { return nickName; }
        }

        public void TimeLeft(int timeLeft)
        {
        }

        public void ItemsToClient(string XMLData)
        {
            return;
        }

        public DynaCommand CommandFromClient(long ticknum)
        {
            return DynaCommand.CmdNothing;
            //return DynaCommand.CmdStop;
            //return (DynaCommand)R.Next(maxNum);
        }

        public BitmapImage TileOfClient()
        {
            return null;
            //return new BitmapImage(new Uri(@"c:\anon\programming\GeekDay5_Dyna_DLL_NONPUB\Images\_anon_.png", UriKind.Absolute));
        }
    }
}
