﻿using System.Globalization;
using System.Xml.Linq;

namespace ArgExClient
{
    public enum PowerUpType
    {
        Simple, NoClip, Armor, SpeedUp, SpeedDown, NoBomb, AlwaysBombs
    }
    public class PowerUp : GameItem
    {
        public PowerUpType Type { get; private set; }

        public PowerUp(XElement element, PowerUpType type) : base(element)
        {
            Type = type;
        }
    }

    public class Wall : GameItem
    {
        //Use the base Property IsExplodable to determine if you can break it
        public Wall(XElement element) : base(element) { }
    }
    public class Space : GameItem
    {
        public Space(XElement element) : base(element) { }
        public Space(int PosX, int PosY) : base()
        {
            ID = -123;
            TileChar = 'Z';
            //IsExplodable = false;
            //IsSolid = false;
            this.PosX = PosX;
            this.PosY = PosY;
            NickName = Settings.General.ClientName;
        }

    }

    //Bomb and Flame are the SAME, only the class name differs them

    public class Bomb : GameItem
    {
        //Inherited from GameItem:
        //public int ID { get; private set; }
        //public char TileChar { get; private set; }
        //public bool IsExplodable { get; private set; }
        //public bool IsSolid { get; private set; }
        //public double PosX { get; private set; }
        //public double PosY { get; private set; }
        //public string NickName { get; private set; }

        public double TimeLeft { get; set; }

        public Bomb(XElement element) : base(element)
        {
            TimeLeft = double.Parse(element.Element("TimeLeft").Value, CultureInfo.InvariantCulture);
        }
        public Bomb(int PosX, int PosY) : base()
        {
            ID = -123;
            TileChar = 'Y';
            //IsExplodable = false;
            //IsSolid = true;
            this.PosX = PosX;
            this.PosY = PosY;
            NickName = Settings.General.ClientName;

            this.TimeLeft = Settings.FromServer.LEVEL.BOMBTIME;
        }
        /// <summary>
        /// Places a virtual (!!!) bomb.
        /// </summary>
        public Bomb() : base()
        {
            ID = Settings.Special.VirtualBombID;
            TileChar = 'Y';
            //IsExplodable = false;
            //IsSolid = true;
            NickName = Settings.General.ClientName;

            this.TimeLeft = Settings.FromServer.LEVEL.BOMBTIME;
        }

        public override string ToString()
        {
            return "BOMB TimeLeft: " + TimeLeft;
        }
    }
    public class Flame : GameItem
    {
        //Inherited from GameItem:
        //public int ID { get; private set; }
        //public char TileChar { get; private set; }
        //public bool IsExplodable { get; private set; }
        //public bool IsSolid { get; private set; }
        //public double PosX { get; private set; }
        //public double PosY { get; private set; }
        //public string NickName { get; private set; }

        public double TimeLeft { get; set; }

        public Flame(XElement element) : base(element)
        {
            TimeLeft = double.Parse(element.Element("TimeLeft").Value, CultureInfo.InvariantCulture);
        }
        public Flame(int PosX, int PosY, double TimeLeft) : base()
        {
            ID = -123;
            TileChar = 'X';
            //IsExplodable = true;
            //IsSolid = false;
            this.PosX = PosX;
            this.PosY = PosY;
            NickName = Settings.General.ClientName;

            this.TimeLeft = TimeLeft;
        }

        public override string ToString()
        {
            return "FLAME TimeLeft: " + TimeLeft;
        }
    }
}