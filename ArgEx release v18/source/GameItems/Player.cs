﻿using System;
using System.Globalization;
using System.Xml.Linq;

namespace ArgExClient
{
    public enum PlayerState
    {
        Standing,
        BetweenTiles,
        InTile
    }
    public class Player : GameItem
    {
        //Inherited from GameItem:
        //public int ID { get; private set; }
        //public char TileChar { get; private set; }
        //public bool IsExplodable { get; private set; }
        //public bool IsSolid { get; private set; }
        //public double PosX { get; private set; }
        //public double PosY { get; private set; }
        //public string NickName { get; private set; }
        //public int DistanceFromUs { get; set; } = int.MaxValue;

        //NEW: (By Arphox)
        public int TilePosX { get; set; }
        public int TilePosY { get; set; }

        //From SERVER:
        public double CurrentWalkSpeed
        {
            get
            {
                double multiplier = 1;
                if (SpeedUpStepsRemaining > 0) multiplier += Settings.FromServer.PLAYER.SPEEDFACTOR;
                if (SpeedDownStepsRemaining > 0) multiplier -= Settings.FromServer.PLAYER.SPEEDFACTOR;
                return WalkSpeed * multiplier;
            }
        }

        //From Player:
        public double WalkSpeed { get; private set; }
        public int BombsAllowed { get; private set; }
        public int BombSize { get; private set; }

        //public int BombsDetonated { get; private set; }
        //public int SecretsCollected { get; private set; }
        public int NoClipStepsRemaining { get; set; }
        public int ArmorStepsRemaining { get; set; }
        public int SpeedUpStepsRemaining { get; set; }
        public int NoBombStepsRemaining { get; set; }
        public int SpeedDownStepsRemaining { get; set; }
        public int AlwaysBombStepsRemaining { get; set; }

        public bool MustStop { get; set; }
        public PlayerState State { get; set; }
        //public int BombsLeft { get; private set; }
        public int DX { get; set; } // [-1 or 0 or  1]
        public int DY { get; set; } // [-1 or 0 or  1]
        //public int Died { get; private set; }       //Number of times the player died TOTALLY (he can Die after FullyDied)
        //public int Kills { get; private set; }      //Number of kills (not counting self kills)

        //public int SelfKills { get; private set; }  //Number of self kills
        public bool IsActive { get; private set; }  //FALSE after:  if (aktPlayer.Died >= LIVES) 
        //public int FullyDied { get; private set; }  //1 after:      if (aktPlayer.Died >= LIVES), otherwise 0
        //public int FullyKilled { get; private set; }//I THINK: 1 If have been killed by OTHERS (not self), otherwise 0

        public Player(XElement element) : base(element)
        {
            TilePosX = (int)Math.Round(PosX, 0);
            TilePosY = (int)Math.Round(PosY, 0);

            WalkSpeed = double.Parse(element.Element("WalkSpeed").Value, CultureInfo.InvariantCulture);
            BombsAllowed = int.Parse(element.Element("BombsAllowed").Value);
            BombSize = int.Parse(element.Element("BombSize").Value);


            NoClipStepsRemaining = int.Parse(element.Element("NoClipStepsRemaining").Value);
            ArmorStepsRemaining = int.Parse(element.Element("ArmorStepsRemaining").Value);
            SpeedUpStepsRemaining = int.Parse(element.Element("SpeedUpStepsRemaining").Value);
            NoBombStepsRemaining = int.Parse(element.Element("NoBombStepsRemaining").Value);
            SpeedDownStepsRemaining = int.Parse(element.Element("SpeedDownStepsRemaining").Value);
            AlwaysBombStepsRemaining = int.Parse(element.Element("AlwaysBombStepsRemaining").Value);

            MustStop = bool.Parse(element.Element("MustStop").Value);
            State = (PlayerState)Enum.Parse(typeof(PlayerState), element.Element("State").Value);
            DX = int.Parse(element.Element("DX").Value);
            DY = int.Parse(element.Element("DY").Value);

            IsActive = bool.Parse(element.Element("IsActive").Value);

            //BombsDetonated = int.Parse(element.Element("BombsDetonated").Value);
            //BombsLeft = int.Parse(element.Element("BombsLeft").Value);
            //Died = int.Parse(element.Element("Died").Value);
            //FullyDied = int.Parse(element.Element("FullyDied").Value);
            //FullyKilled = int.Parse(element.Element("FullyKilled").Value);
            //Kills = int.Parse(element.Element("Kills").Value);
            //SecretsCollected = int.Parse(element.Element("SecretsCollected").Value);
            //SelfKills = int.Parse(element.Element("SelfKills").Value);
        }

        public override string ToString()
        {
            return string.Format("{0} X:{2} Y:{3} [{1}]", NickName, ID, PosX, PosY);
        }
    }
}