﻿using System;

namespace ArgExClient.GameLogic.WaveFrontExpansion
{
    public class WaveFrontExpAlg
    {
        //WARNING:
        //Wavefront Expansion algorithm will ignore targets at the border (in this case returns int.MaxValue for the Distance)

        public int[,] CurrentWaveMap = new int[Settings.General.MapSizeX, Settings.General.MapSizeY];

        Map map;

        int myX;
        int myY;

        int TargetX;
        int TargetY;

        bool firstTime = true;
        bool reached = false;
        int cellCount = 0;

        public WaveFrontExpAlg(Map map, GameItem target)
        {
            this.map = map;
            myX = map.MyPlayer.TilePosX;
            myY = map.MyPlayer.TilePosY;


            Player targetPlayer = target as Player;
            if (targetPlayer != null)
            {
                TargetX = targetPlayer.TilePosX;
                TargetY = targetPlayer.TilePosY;
            }
            else
            {
                TargetX = (int)target.PosX;
                TargetY = (int)target.PosY;
            }

            #region LoadMap
            for (int i = 0; i < Settings.General.MapSizeX; i++)
            {
                for (int j = 0; j < Settings.General.MapSizeY; j++)
                {
                    if (map[i, j].CanWalkThrough)
                        CurrentWaveMap[i, j] = 0;
                    else
                        CurrentWaveMap[i, j] = -1;
                }
            }

            CurrentWaveMap[myX, myY] = -2; //Me
            CurrentWaveMap[TargetX, TargetY] = -3;     //Target
            #endregion
        }
        public int Distance()
        {
            //If the target or me at the border, we won't calculate with it
            if (TargetX == 0 || TargetY == 0 || TargetX == Settings.General.MapSizeX - 1 || TargetY == Settings.General.MapSizeY - 1 ||
                myX == 0 || myY == 0 || myX == Settings.General.MapSizeX - 1 || myY == Settings.General.MapSizeY - 1)
                return int.MaxValue;

            do
            {
                #region WaveExpansion
                int[,] copy = (int[,])CurrentWaveMap.Clone();
                cellCount = 0;

                int minValWaveExp = int.MinValue;

                #region First
                if (firstTime)
                {
                    firstTime = false;

                    if (CurrentWaveMap[TargetX - 1, TargetY] == 0) copy[TargetX - 1, TargetY] = 1;
                    if (CurrentWaveMap[TargetX + 1, TargetY] == 0) copy[TargetX + 1, TargetY] = 1;
                    if (CurrentWaveMap[TargetX, TargetY - 1] == 0) copy[TargetX, TargetY - 1] = 1;
                    if (CurrentWaveMap[TargetX, TargetY + 1] == 0) copy[TargetX, TargetY + 1] = 1;

                    cellCount = 4;
                }
                #endregion
                #region Rest
                else
                {
                    for (int i = 1; i < CurrentWaveMap.GetLength(0) - 1; i++)
                    {
                        for (int j = 1; j < CurrentWaveMap.GetLength(1) - 1; j++)
                        {
                            if (CurrentWaveMap[i, j] == 0)
                            {
                                //Is there a wavefront next to him (right/left) or above/under?
                                if (CurrentWaveMap[i + 1, j] > 0 ||
                                    CurrentWaveMap[i - 1, j] > 0 ||
                                    CurrentWaveMap[i, j + 1] > 0 ||
                                    CurrentWaveMap[i, j - 1] > 0
                                    )
                                {
                                    //If YES, find minimum value:
                                    minValWaveExp = int.MaxValue;
                                    if (CurrentWaveMap[i + 1, j] > 0 && CurrentWaveMap[i + 1, j] < minValWaveExp) minValWaveExp = CurrentWaveMap[i + 1, j];
                                    if (CurrentWaveMap[i - 1, j] > 0 && CurrentWaveMap[i - 1, j] < minValWaveExp) minValWaveExp = CurrentWaveMap[i - 1, j];
                                    if (CurrentWaveMap[i, j + 1] > 0 && CurrentWaveMap[i, j + 1] < minValWaveExp) minValWaveExp = CurrentWaveMap[i, j + 1];
                                    if (CurrentWaveMap[i, j - 1] > 0 && CurrentWaveMap[i, j - 1] < minValWaveExp) minValWaveExp = CurrentWaveMap[i, j - 1];

                                    copy[i, j] = minValWaveExp + 1;
                                    cellCount = 1;
                                }
                            }
                        }
                    }
                }
                #endregion

                CurrentWaveMap = copy;
                #endregion

                //if reached
                if (CurrentWaveMap[myX, myY - 1] > 0) { reached = true; break; } //UP
                if (CurrentWaveMap[myX, myY + 1] > 0) { reached = true; break; } //DOWN
                if (CurrentWaveMap[myX - 1, myY] > 0) { reached = true; break; } //LEFT
                if (CurrentWaveMap[myX + 1, myY] > 0) { reached = true; break; } //RIGHT

            } while (cellCount > 0 && !reached);

            //Logger.LogIntMap(CurrentWaveMap); //Uncomment if need log

            //Distance = Environment's (Left, Right, Above, Under) minimum value + 1
            int minDistance = int.MaxValue;
            if (CurrentWaveMap[myX + 1, myY] > 0 && CurrentWaveMap[myX + 1, myY] < minDistance) minDistance = CurrentWaveMap[myX + 1, myY];
            if (CurrentWaveMap[myX - 1, myY] > 0 && CurrentWaveMap[myX - 1, myY] < minDistance) minDistance = CurrentWaveMap[myX - 1, myY];
            if (CurrentWaveMap[myX, myY + 1] > 0 && CurrentWaveMap[myX, myY + 1] < minDistance) minDistance = CurrentWaveMap[myX, myY + 1];
            if (CurrentWaveMap[myX, myY - 1] > 0 && CurrentWaveMap[myX, myY - 1] < minDistance) minDistance = CurrentWaveMap[myX, myY - 1];

            //If we are next to the target:
            if (CurrentWaveMap[myX + 1, myY] == -3 || CurrentWaveMap[myX - 1, myY] == -3 ||
                CurrentWaveMap[myX, myY + 1] == -3 || CurrentWaveMap[myX, myY - 1] == -3)
                minDistance = 1;

            return minDistance;
        }
    }
}